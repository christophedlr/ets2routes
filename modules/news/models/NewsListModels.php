<?php

namespace Modules\News;

use Core\AppModels;

/**
 * News list model
 * 
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @author Christophe Daloz - De Los Rios <christophedlr@gmail.com>
 * @copyright ETS2Routes Dev Team 2016
 * @uses Core\AppModels Application principal models
 */
class NewsListModels extends AppModels {
	/**
	 * Get of three last news
	 *
	 * @return array <div>Array list of news or empty arrya</div>
	 */
	public function getThreeLastNews() {
		$db = $this->db;
		$query = $db->query ( 'SELECT `news`.*, `news_cat`.`name` AS `cat_name`,
			`news_cat`.`id_cat`, `users`.`login` AS `user` FROM `news` INNER JOIN `news_cat`
			ON `news`.`id_cat` = `news_cat`.`id_cat`
			INNER JOIN `users` ON `news`.`id_user` = `users`.`id_users` ORDER BY `id_news` DESC LIMIT 3 OFFSET 0' );
		if ($query->execute ()) {
			return $query->fetchAll ( $db::FETCH_ASSOC );
		}
		
		return array ();
	}
	
	/**
	 * Get list of news
	 *
	 * @return array <div>Array list of news or empty arrya</div>
	 */
	public function getNewsList() {
		$db = $this->db;
		$query = $db->query ( 'SELECT `news`.*, `news_cat`.`name` AS `cat_name`,
			`news_cat`.`id_cat`, `users`.`login` AS `user` FROM `news` INNER JOIN `news_cat`
			ON `news`.`id_cat` = `news_cat`.`id_cat`
			INNER JOIN `users` ON `news`.`id_user` = `users`.`id_users` ORDER BY `date` ASC' );
		if ($query->execute ()) {
			return $query->fetchAll ( $db::FETCH_ASSOC );
		}
	
		return array ();
	}
	
	/**
	 * Get list of categories
	 * 
	 * @return stdClass <div>List of categories of news</div>
	 */
	public function getCatList() {
		$db = $this->db;
		$query = $db->query('SELECT * FROM `news_cat`');
		if ( $query->execute() ) {
			return $query->fetchAll( $db::FETCH_OBJ );
		}
		
		return new \stdClass;
	}
	
	public function addNews($post) {
		if ( !empty($post->category) && !empty($post->user) && !empty($post->title) && !empty($post->message) ) {
			$db = $this->db;
			$query = $db->prepare('INSERT INTO `news` (`id_cat`, `id_user`, `title`, `message`, `date`) VALUES
			(:category, :user, :title, :message, NOW())');
			
			$category = intval($post->category);
			$user = intval($post->user);
			
			$query->bindParam(':category', $category, $db::PARAM_INT);
			$query->bindParam(':user', $user, $db::PARAM_INT);
			$query->bindParam(':title', $post->title);
			$query->bindParam(':message', $post->message);
			
			if ( $query->execute() ) {
				return true;
			}
			
			return false;
		}
		
		return false;
	}
	
	public function getNews($id) {
		$db = $this->db;
		$query = $db->prepare('SELECT `news`.*, `news_cat`.`name` AS `cat_name` FROM `news`
			INNER JOIN `news_cat` ON `news`.`id_cat` = `news_cat`.`id_cat` WHERE `id_news` = :id');
		$query->bindParam(':id', $id, $db::PARAM_INT);
		
		if ( $query->execute() ) {
			return $query->fetch($db::FETCH_OBJ);
		}
		
		return \stdClass;
	}
	
	public function editNews($post) {
		if ( !empty($post->category) && !empty($post->title) && !empty($post->message) ) {
			$db = $this->db;
			$query = $db->prepare('UPDATE `news` SET `title` = :title, `id_cat` = :category, `message` = :message 
				WHERE `id_news` = :id_news');
			
			$query->bindParam(':title', $post->title);
			$query->bindParam(':category', $post->category, $db::PARAM_INT);
			$query->bindParam(':message', $post->message);
			$query->bindParam(':id_news', $post->id_news, $db::PARAM_INT);
			
			if ( $query->execute() ) {
				return true;
			}
			
			return false;
		}
		
		return false;
	}
	
	public function delNews($id) {
		$db = $this->db;
		$query = $db->prepare('DELETE FROM `news` WHERE `id_news` = :id');
		$query->bindParam(':id', $id, $db::PARAM_INT);
		
		if ( $query->execute() ) {
			return true;
		}
		
		return false;
	}
	
	public function addCat($post) {
		$db = $this->db;
		$query = $db->prepare('INSERT INTO `news_cat` (`name`) VALUES(:name)');
		$query->bindParam(':name', $post->name, $db::PARAM_INT);
		
		if ( $query->execute() ) {
			return true;
		}
		
		return false;
	}
	
	public function editCat($post) {
		if ( !empty($post->name) && !empty($post->id_cat) ) {
			$db = $this->db;
			$query = $db->prepare('UPDATE `news_cat` SET `name` = :name WHERE `id_cat` = :id_cat');
			$query->bindParam(':name', $post->name);
			$query->bindParam('id_cat', $post->id_cat, $db::PARAM_INT);
			
			if ( $query->execute() ) {
				return true;
			}
			
			return false;
		}
		
		return false;
	}
	
	public function getCat($id) {
		$db = $this->db;
		$query = $db->prepare('SELECT * FROM `news_cat` WHERE `id_cat`= :id');
		$query->bindParam(':id', $id, $db::PARAM_INT);
		
		if ( $query->execute() ) {
			return $query->fetch($db::FETCH_OBJ);
		}
		
		return \stdClass;
	}
	
	public function delCat($id) {
		$db = $this->db;
		$query = $db->prepare('DELETE FROM `news_cat` WHERE `id_cat` = :id');
		$query->bindParam(':id', $id, $db::PARAM_INT);
		
		if ( $query->execute() ) {
			return true;
		}
		
		return false;
	}
}
