<?php

namespace Modules\News;

use Core\AppController;

/**
 * Administration News controller
 * 
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @author Christophe Daloz - De Los Rios <christophedlr@gmail.com>
 * @copyright ETS2Routes Dev Team 2016
 * @uses Core\AppController Application principal controller
 */
class AdminNewsController extends AppController {
	private $newsListModels;
	
	public function __construct() {
		parent::__construct ();
		$this->twig->addTwigBaseDir ( 'modules/news', 'News' );
		$this->twig->addTwigBaseDir ( 'app/admin', 'Admin' );
		
		$this->newsListModels = new NewsListModels();
	}
	
	/**
	 * Index action
	 */
	public function indexAction($args) {
		switch ( $args->type ) {
			case 'add':
				$this->addNews();
			break;
			
			case 'edit':
				$this->editNews($args->id);
			break;
			
			case 'del':
				$this->delNews($args->id);
			break;
			
			case 'cat':
				echo $this->twigRender('@News/adminCatList.twig', array(
					'list' => $this->newsListModels->getCatList()
				));
			break;
			
			case 'addcat':
				$this->addCat();
			break;
			
			case 'editcat':
				$this->editCat($args->id);
			break;
			
			case 'delcat':
				$this->delCat($args->id);
			break;
			
			default:
				echo $this->twigRender('@News/adminList.twig', array(
						'list' => $this->newsListModels->getNewsList()
				));
			break;
		}
	}
	
	private function addNews() {
		if ( !empty($_POST) ) {
			$post = $_POST;
			$post['user'] = $this->users->getUser()->id;
			$post = (object)$post;
			
			$result = $this->newsListModels->addNews($post);
			
			if ( $result ) {
				$message['type'] = 'success';
				$message['msg'] = 'La news <b>'.$post->title.'</b> a bien été ajoutée';
				$this->users->addTempVar('message', $message);
				$this->redirect( array('admin', 'news') );
			}
			else {
				$message['type'] = 'danger';
				$message['msg'] = 'Soit la news n\'a pu être enregistrée, soit vous n\'avez pas remplis tout les champs';
				
				echo $this->twigRender('@News/adminNewsAdd.twig', array(
					'categories' => $this->newsListModels->getCatList(),
					'message' => $message,
					'post' => $post
				));
			}
		}
		else {
			echo $this->twigRender('@News/adminNewsAdd.twig', array(
				'categories' => $this->newsListModels->getCatList()
			));
		}
	}
	
	private function editNews($id) {
		if ( !empty($_POST) ) {
			$post = $_POST;
			$post['id_news'] = $id;
			$post['id_cat'] = $post['category'];
			$post = (object)$post;
			
			$result = $this->newsListModels->editNews($post);
			
			if ( $result ) {
				$message['type'] = 'success';
				$message['msg'] = 'La news <b>'.$post->title.'</b> a bien été modifiée';
				$this->users->addTempVar('message', $message);
				$this->redirect( array('admin', 'news') );
			}
			else {
				$message['type'] = 'danger';
				$message['msg'] = 'Soit la news n\'a pu être modifiée, soit vous n\'avez pas remplis tout les champs';
				
				echo $this->twigRender('@News/adminNewsEdit.twig', array(
					'post' => $post,
					'categories' => $this->newsListModels->getCatList(),
					'message' => $message,
				));
			}
		}
		else {
			$news = $this->newsListModels->getNews($id);
			
			echo $this->twigRender('@News/adminNewsEdit.twig', array(
				'post' => $news,
				'categories' => $this->newsListModels->getCatList()
			));
		}
	}
	
	private function delNews($id) {
		$result = $this->newsListModels->delNews($id);
		
		if ( $result ) {
			$message['type'] = 'success';
			$message['msg'] = 'La news numéro <b>'.$id.'</b> a bien été supprimée';
			
			$this->users->addTempVar('message', $message);
			$this->redirect( array('admin', 'news') );
		}
		else {
			$message['type'] = 'danger';
			$message['msg'] = 'La news numéro <b>'.$id.'</b> n\'a pas été supprimée';
			$this->users->addTempVar('message', $message);
			$this->redirect( array('admin', 'news') );
		}
	}
	
	private function addCat() {
		if ( !empty($_POST) ) {
			$post = $_POST;
			$post = (object)$post;
				
			$result = $this->newsListModels->addCat($post);
			
			if ( $result ) {
				$message['type'] = 'success';
				$message['msg'] = 'La catégorie <b>'.$post->name.'</b> a bien été ajoutée';
				$this->users->addTempVar('message', $message);
				$this->redirect( array('admin', 'news', 'cat') );
			}
			else {
				$message['type'] = 'danger';
				$message['msg'] = 'Soit la catégorie n\'a pu être enregistrée, soit vous n\'avez pas remplis tout les champs';
				
				echo $this->twigRender('@News/adminCatAdd.twig', array(
					'post' => $post
				));
			}
		}
		else {
			echo $this->twigRender('@News/adminCatAdd.twig', array(
			));
		}
	}
	
	private function editCat($id) {
		if ( !empty($_POST) ) {
			$post = $_POST;
			$post['id_cat'] = $id;
			$post = (object)$post;
		
			$result = $this->newsListModels->editCat($post);
				
			if ( $result ) {
				$message['type'] = 'success';
				$message['msg'] = 'La catégorie <b>'.$post->name.'</b> a bien été ajoutée';
				$this->users->addTempVar('message', $message);
				$this->redirect( array('admin', 'news', 'cat') );
			}
			else {
				$message['type'] = 'danger';
				$message['msg'] = 'Soit la catégorie n\'a pu être enregistrée, soit vous n\'avez pas remplis tout les champs';
		
				echo $this->twigRender('@News/adminCatEdit.twig', array(
						'post' => $post
				));
			}
		}
		else {
			echo $this->twigRender('@News/adminCatEdit.twig', array(
				'post' => $this->newsListModels->getCat($id)
			));
		}
	}
	
	private function delCat($id) {
		$result = $this->newsListModels->delCat($id);
	
		if ( $result ) {
			$message['type'] = 'success';
			$message['msg'] = 'La catégorie numéro <b>'.$id.'</b> a bien été supprimée';
				
			$this->users->addTempVar('message', $message);
			$this->redirect( array('admin', 'news', 'cat') );
		}
		else {
			$message['type'] = 'danger';
			$message['msg'] = 'La catégorie numéro <b>'.$id.'</b> n\'a pas été supprimée';
			$this->users->addTempVar('message', $message);
			$this->redirect( array('admin', 'news', 'cat') );
		}
	}
}
