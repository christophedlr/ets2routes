<?php

namespace Modules\News;

use Core\AppController;

/**
 * News controller
 * 
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @author Christophe Daloz - De Los Rios <christophedlr@gmail.com>
 * @copyright ETS2Routes Dev Team 2016
 * @uses Core\AppController Application principal controller
 */
class NewsController extends AppController {
	public function __construct() {
		parent::__construct ();
		$this->twig->addTwigBaseDir ( 'modules/news', 'News' );
		
		$this->hook->addAction ( 'index_css', ABSOLUTE_PATH . '/modules/news/' . CSS_PATH . '/global.css' );
		$this->hook->addAction ( 'index_tpl', array (
				$this, 'indexAction'), 'after' );
	}
	
	/**
	 * Index action
	 *
	 * @return string <p>News template</p>
	 */
	public function indexAction() {
		$newsModel = new NewsListModels ();
		$list = $newsModel->getThreeLastNews ();
		
		$template = $this->twigRender ( '@News/list.twig', array (
				'news' => $list 
		) );
		
		return $template;
	}
}
