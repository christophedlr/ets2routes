<?php

namespace Modules\Faq;

use Core\AppController;
use Core\Users\SessionController;

/**
 * FAQ controller
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @author Christophe Daloz - De Los Rios <christophedlr@gmail.com>
 * @copyright ETS2Routes Dev Team 2016
 * @uses Core\AppController Aplication principal controller
 */
class FaqController extends AppController {
	public function __construct() {
		parent::__construct();
		$this->twig->addTwigBaseDir('modules/faq', 'Faq');
	}
	
	/**
	 * Index action
	 */
	public function indexAction() {
		$questionList = new QuestionListModels();
		
		echo $this->twigRender('@Faq/index.twig', array(
				//'title' => $this->modules->getName( basename( dirname(__DIR__) ) ),
				'list' => $questionList->getAll()
		));
	}
	
	public function adminAction(\stdClass $args = NULL) {
		$message = array();
		
		if ( is_null($args) || $args->type === 'faq') {
			if ( is_null($args->page) ) {
				$questionList = new QuestionListModels();
			
				echo $this->twigRender('@Faq/admin/list.twig', array(
						'questions' => $questionList->getAll(),
						'message' => $this->users->getTempVar('message')
				));
			}
			else if ( substr( $args->page, 0, strpos($args->page, '-') ) === 'edit' ) {
				$id = intval( substr( $args->page, strpos($args->page, '-')+1 ) );
				
				if ( !empty($_POST) ) {
					if ( !empty($_POST['question']) && !empty($_POST['response']) ) {
						$_POST['id_faq'] = $id;
						
						$questionList = new QuestionListModels();
						$result = $questionList->changeQuestion($_POST);
						
						if ( $result ) {
							$message['type'] = 'success';
							$message['msg'] = 'La question numéro <b>'.$id.'</b> a bien été modifiée';
						}
						else {
							$message['type'] = 'danger';
							$message['msg'] = 'Impossible de modifier la question numéro <b>'.$id.'</b>';
							
							echo $this->twigRender('@Faq/admin/edit.twig', array(
									'faq' => $_POST
							));
						}
						
						$this->users->addTempVar('message', $message);
						$this->redirect(array('admin','faq'));
					}
				}
				else {
					$questionList = new QuestionListModels();
					$questions = $questionList->getQuestion( $id );
						
					echo $this->twigRender('@Faq/admin/edit.twig', array(
							'faq' => $questions
					));
				}
			}
			else if ( substr( $args->page, 0, strpos($args->page, '-') ) === 'del' ) {
				$questionList = new QuestionListModels();
				$id = intval( substr( $args->page, strpos($args->page, '-')+1 ) );
				$result = $questionList->delQuestion( $id );
				
				if ( $result ) {
					$message['type'] = 'success';
					$message['msg'] = 'La question numéro <b>'.$id.'</b> a bien été supprimée';
				}
				else {
					$message['type'] = 'danger';
					$message['msg'] = 'Impossible de supprimer la question numéro <b>'.$id.'</b>';
				}
				
				$this->users->addTempVar('message', $message);
				$this->redirect(array('admin','faq'));
			}
			else if ( $args->page === 'add' ) {
				if ( !empty($_POST) ) {
					$questionModels = new QuestionListModels();
					$result = $questionModels->addQuestion($_POST);
					
					if ( $result ) {
						$message['type'] = 'success';
						$message['msg'] = 'La question a bien été ajouté';
						$this->users->addTempVar('message', $message);
						$this->redirect(array('admin','faq'));
					}
					else {
						$message['type'] = 'danger';
						$message['msg'] = 'Impossible d\'ajouter la question';
					}
				}
				
				echo $this->twigRender('@Faq/admin/add.twig', array(
					'faq' => $_POST,
					'message' => $message
				));
			}
		}
	}
}
