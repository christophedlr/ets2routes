<?php

namespace Modules\Faq;

use Core\AppModels;

/**
 * FAQ questions list model
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @author Christophe Daloz - De Los Rios <christophedlr@gmail.com>
 * @copyright ETS2Routes Dev Team 2016
 * @uses Core\AppModels Application principal model
 */
class QuestionListModels extends AppModels {
	/**
	 * Get all questions
	 * 
	 * @return array
	 * <div>Array list of questions/responses or empty array</div>
	 */
	public function getAll() {
		$db = $this->db;
		$query = $this->db->query('SELECT * FROM `faq`');
		
		if ( $query->execute() ) {
			return $query->fetchAll($db::FETCH_ASSOC);
		}
		
		return array();
	}
	
	public function getQuestion($id) {
		$db = $this->db;
		$statement = 'SELECT * FROM `faq` WHERE `id_faq` = :id';
		
		$query = $this->db->prepare($statement);
		$query->bindParam(':id', $id, $db::PARAM_INT);
		
		if ( $query->execute() ) {
			return $query->fetch($db::FETCH_OBJ);
		}
	}
	
	public function changeQuestion($data) {
		$db = $this->db;
		$statement = 'UPDATE `faq` SET `question` = :question, `response`= :response WHERE `id_faq` = :id';
		
		$query = $this->db->prepare($statement);
		$query->bindParam(':id', $data['id_faq'], $db::PARAM_INT);
		$query->bindParam(':question', $data['question']);
		$query->bindParam(':response', $data['response']);
		
		return $query->execute();
	}
	
	public function addQuestion($data) {
		$statement = 'INSERT INTO `faq` VALUES("",:question, :response)';
	
		$query = $this->db->prepare($statement);
		$query->bindParam(':question', $data['question']);
		$query->bindParam(':response', $data['response']);
	
		return $query->execute();
	}
	
	public function delQuestion($id) {
		$db = $this->db;
		$statement = 'DELETE FROM `faq` WHERE `id_faq` = :id';
		
		$query = $this->db->prepare($statement);
		$query->bindParam(':id', $id, $db::PARAM_INT);
		
		return $query->execute();
	}
}
