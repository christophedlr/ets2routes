# ETS2Routes

## Qu'est ce que c'est ?

ETS2Routes est un site internet sur le jeu Euro Truck Simulator 2. Ce site vous permet de vous
immerger un peut plus dans le jeu en apportant un système de gestion d'entreprise. En prime
il vous est possible d'être plusieurs joueurs dans l'entreprise, de quoi se lancer des défis.

## Comment on l'utilise ?

Chaque membre d'une entreprise remplis un rapport de trajet, sorte de feuille de route mais une
fois que le trajet est terminé (les feuilles de routes ont pour princie de définir le trajet à l'avance).

## Installation

L'installation du site est très simple, vous récupérez les fichiers sur le dépôt ou le site officiel.
Quand cest fait, vous transférez chez votre hébergeur. Quand vous vous connecterez au site,
ce dernier vous demandera dans un formulaire, les informations de connexion.
Vous pouvez aussi réinstaller le site en saisissant dans votre barre d'adresse : /install quand vous êtes
à l'accueil du site, utile si par exemple vous avez eu malencontreusement un fichier de configuration
erronée.

Sachez cependant que la réinstallation, procède à la recréation du fichier de configuration,
la suppression de toutes les tables présentes et donc toutes les données correspondant au site.

## Site officiel

Vous pouvez vous rendre sur le site officiel : https://www.ets2routes.com et utiliser le site
directement depuis cet endroit, comme vous pouvez installer une copie chez votre hébergeur ou
votre ordinateur.

Le projet est sous la licence libre CeCILL V2, vous avez donc le droit de modifier et adapter le code,
toutefois vous devez placer toutes modification sous la même licence ou une licence compatible, voir
le site http://www.cecill.info pour plus d'informations.

ETS2Routes est un projet totalement gratuit, à aucun moment il vous sera demandé de payer quelque chose.
