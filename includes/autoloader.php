<?php

function autoloader($class) {
	$file = '';
	$namespace = '';
	$explode = explode('\\', $class);
	
	$index = count($explode);
	
	for ($i = 0; $i < $index; $i++) {
		if ( $i === $index-1 ) {
			$file = $explode[$i];
		}
		else {
			$namespace .= strtolower($explode[$i].'/');
		}
	}
	
	if ( substr($file, -10) === 'Controller' ) {
		$type = 'controller/';
	}
	else if ( substr($file, -6) === 'Models' ) {
		$type = 'models/';
	}
	
	$namespace .= $type;
	$namespace = str_replace('core', 'app', $namespace);
	$require = $namespace.$file.'.php';
	
	require_once $require;
}

spl_autoload_register('autoloader');
