<?php

define('ASSETS_PATH', 'assets');
define('LIBS_PATH', 'vendor');
define('CSS_PATH', ASSETS_PATH.'/css');
define('JS_PATH', ASSETS_PATH.'/js');
define('IMG_PATH', ASSETS_PATH.'/images');
define('APP_VIEWS_PATH', 'app/views');
define('ABSOLUTE_PATH', ( ( isset($_SERVER['HTTPS']) ) ? 'https://' : 'http://' ).
		$_SERVER['SERVER_NAME'].dirname($_SERVER['SCRIPT_NAME'] ) );
define('LOCAL_PATH', dirname($_SERVER['SCRIPT_FILENAME']));
