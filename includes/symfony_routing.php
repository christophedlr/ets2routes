<?php

/*use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\YamlFileLoader;

$locator = new FileLocator( array(LOCAL_PATH) );
$loader = new YamlFileLoader($locator);
$collection = $loader->load('routes.yml');
var_dump($collection);*/

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Generator\UrlGenerator;

/*$collection = new RouteCollection();
$collection->add('blog_show', new Route('/test', array(
		'_controller' => 'AppBundle:Blog:show',
)));*/

$locator = new FileLocator( array(LOCAL_PATH) );
$loader = new YamlFileLoader($locator);
$collection = $loader->load('routes.yml');

$context = new RequestContext();
//$context->fromRequest($request);
$matcher = new UrlMatcher($collection, $context);
$urlGenerator = new UrlGenerator($collection, $context);

//$matcher->match('/sq');
//var_dump($matcher->match('/')); exit;

return $collection;