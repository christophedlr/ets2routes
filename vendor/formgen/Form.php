<?php

/*
 * Copyright Christophe Daloz - De Los Rios 2016
 *
 * christophedlr@gmail.com
 *
 * This software is a computer program whose purpose is to website generate
 * a various form in HTML.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. UsersModel are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/**
 * Form generator
 * @author Christophe DALOZ - DE LOS RIOS <christophedlr@gmail.com>
 * @copyright 2016 Christophe Daloz - De Los Rios
 * @license <a href="http://cecill.info/licences/Licence_CeCILL_V2-en.html">CeCILL V2</a>
 * @version 3.0
 */
class Form {
	private $dom;
	
	public function __construct() {
		$this->dom = new DOMDocument();
	}
	
	/**
	 * Open existing form
	 * @param string $filename
	 * <div>Name of file</div>
	 */
	public function open($filename) {
		$this->dom->loadHTMLFile($filename, LIBXML_HTML_NODEFDTD | LIBXML_HTML_NOIMPLIED | LIBXML_NOENT);
	}
	
	/**
	 * Create new form
	 * <div>If existing form(s), the form is added after all other forms</div>
	 * 
	 * @param string $action
	 * <div>The action link of form</div>
	 * 
	 * @param string $method
	 * <div>The method request HTML protocol of form</div>
	 * 
	 * @return DOMElement
	 * <div>The form create</div>
	 */
	public function create($action, $method) {
		$form = $this->createNode('form');
		$form->setAttribute('action', $action);
		$form->setAttribute('method', $method);
		
		$this->dom->appendChild($form);
		return $form;
	}
	
	/**
	 * Save the actual form on disk
	 * @param string $filename
	 * <div>Name of file</div>
	 */
	public function saveOnDisk($filename) {
		file_put_contents($filename,  $this->render() );
	}
	
	/**
	 * Get all elements with tag indicate
	 * @param string $name
	 * <div>Name of tag</div>
	 * 
	 * @return DOMNodeList
	 * <div>List of nodes selected</div>
	 */
	public function getElementsByTagName($name) {
		return $this->dom->getElementsByTagName($name);
	}
	
	/**
	 * Get node with id attribute indicate
	 * @param string $id
	 * <div>Name of id attribute</div>
	 * 
	 * @return DOMNode
	 * <div>Node selected</div>
	 */
	public function getElementById($id) {
		return $this->dom->getElementById($id);
	}
	
	/**
	 * Create a new node
	 * @param string $name
	 * <div>Name of childe node</dv>
	 * 
	 * @param string $value
	 * <div>By default, value is empty string</div>
	 * 
	 * @return DOMElement
	 * <div>New node</div>
	 */
	public function createNode($name, $value = '') {
		$newNode = $this->dom->createElement($name, $value);
		
		return $newNode;
	}
	
	/**
	 * Remove selected child node in selected node
	 * @param DOMElement $node
	 * <div>Selected node</div>
	 * 
	 * @param DOMNode $old
	 * <div>Node at delete</div>
	 */
	public function removeChildNode($node, $old) {
		$node->removeChild($old);
	}
	
	/**
	 * Insert element before selected node
	 * @param DOMElement $node
	 * <div>Selected node</div>
	 * 
	 * @param DOMElement $element
	 * <div>New nod</div>
	 */
	public function insertBefore($node, $element) {
		$node->parentNode->insertBefore($element, $node);
	}
	
	/**
	 * Insert element after selected node
	 * @param DOMElement $node
	 * <div>Selected node</div>
	 * 
	 * @param DOMElement $element
	 * <div>New nod</div>
	 */
	public function insertAfter($node, $element) {
		$node->parentNode->insertBefore($element, $node->nextSibling);
	}
	
	/**
	 * Add new child in selected node
	 * @param DOMElement $node
	 * <div>Selected node</div>
	 * 
	 * @param DOMElement $element
	 * <div>New nod</div>
	 */
	public function appendChild($node, $element) {
		$node->appendChild($element);
	}

	/**
	 * Set attribute
	 * @param DOMElement $node
	 * <div>Selected node</div>
	 * 
	 * @param string $name
	 * <div>Name of attribute</div>
	 * 
	 * @param string $value
	 * <div>Value of attribute</div>
	 */
	public function setAttribute($node, $name, $value) {
		$node->setAttribute($name, $value);
	}
	
	/**
	 * Remove attribute
	 * @param DOMElement $node
	 * <div>Selected node</div>
	 * 
	 * @param string $name
	 * <div>Name of attribute</div>
	 */
	public function removeAttribute($node, $name) {
		$node->removeAttribute($name);
	}
	
	/**
	 * Get attribute
	 * @param DOMElement $node
	 * <div>Selected node</div>
	 * 
	 * @param string $name
	 * <div>Name of attribute</div>
	 * 
	 * @return string
	 * <div>The value of attribute or empty</div>
	 */
	public function getAttribute($node, $name) {
		return $node->getAttribute($name);
	}
	
	/**
	 * Render HTML form
	 * @return string
	 * <div>Content of HTML form</div>
	 */
	public function render() {
		return $this->dom->saveHTML();
	}
}
