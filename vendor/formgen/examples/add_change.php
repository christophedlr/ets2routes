<?php
require_once '../Form.php';
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Example - Add and change fields</title>
	</head>
	
	<body>
		<h1>Add new field and modify attribute of exist field</h1>
		<?php
		$form = new Form();
		$form->open('base_form.html');
		
		$testNode = $form->getElementById('test');
		$form->setAttribute($testNode, 'value', 'I\'m a little test');
		
		$newNode = $form->createNode('button', 'Ok');
		$form->insertAfter($testNode, $newNode);
		
		$newNode = $form->createNode('input');
		$newNode->setAttribute('name', 'test2');
		$newNode->setAttribute('placeholder', 'test2');
		$newNode->setAttribute('type', 'text');
		
		$form->insertAfter($testNode, $newNode);
		
		echo $form->render();
		?>
	</body>
</html>
