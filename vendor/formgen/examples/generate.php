<?php

require_once '../Form.php';

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
			integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
			crossorigin="anonymous">
		<title>Example - Generate new form</title>
	</head>
	
	<body class="container" style="margin: 1%">
		<?php
		$form = new Form();
		$newForm = $form->create('generate.php', 'POST');
		$form->setAttribute($newForm, 'class', 'form-horizontal');
		
		/*Create login input*/
		$div = $form->createNode('div');
		$divInput = $form->createNode('div');
		
		$div->setAttribute('class', 'form-group');
		
		$label = $form->createNode('label', 'Login');
		$label->setAttribute('for', 'login');
		$label->setAttribute('class', 'col-sm-2 control-label');
		
		$input = $form->createNode('input');
		$input->setAttribute('type', 'text');
		$input->setAttribute('name', 'login');
		$input->setAttribute('id', 'login');
		$input->setAttribute('placeholder', 'Your login');
		$input->setAttribute('class', 'form-control');
		
		$divInput->setAttribute('class', 'col-sm-10');
		$divInput->appendChild($input);
		
		$div->appendChild($label);
		$div->appendChild($divInput);
		
		$form->appendChild( $form->getElementsByTagName('form')->item(0) , $div);
		
		
		/*Create password input*/
		$div = $form->createNode('div');
		$divInput = $form->createNode('div');
		
		$div->setAttribute('class', 'form-group');
		
		$label = $form->createNode('label', 'Password');
		$label->setAttribute('for', 'password');
		$label->setAttribute('class', 'col-sm-2 control-label');
		
		$input = $form->createNode('input');
		$input->setAttribute('type', 'password');
		$input->setAttribute('name', 'password');
		$input->setAttribute('id', 'password');
		$input->setAttribute('placeholder', 'Your password');
		$input->setAttribute('class', 'form-control');
		
		$divInput->setAttribute('class', 'col-sm-10');
		$divInput->appendChild($input);
		
		$div->appendChild($label);
		$div->appendChild($divInput);
		
		$form->appendChild( $form->getElementsByTagName('form')->item(0) , $div);
		
		/*Create submit button*/
		$div = $form->createNode('div');
		$divButton = $form->createNode('div');
		
		$div->setAttribute('class', 'form-group');
		
		$button = $form->createNode('button', 'Sign in');
		$button->setAttribute('type', 'submit');
		$button->setAttribute('class', 'btn btn-default');
		
		$divButton->setAttribute('class', 'col-sm-offset-2 col-sm-10');
		$divButton->appendChild($button);
		
		$div->appendChild($divButton);
		
		$form->appendChild( $form->getElementsByTagName('form')->item(0) , $div);
		
		echo $form->render();
		?>
	</body>
</html>