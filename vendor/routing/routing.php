<?php

/*
 * Copyright Christophe DALOZ - DE LOS RIOS 2016
 * 
 * christophedlr@gmail.com
 * 
 * This software is a computer program whose purpose is to manager of routes.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/**
 * Manager of routes
 * @author Christophe DALOZ - DE LOS RIOS <christophedlr@gmail.com>
 * @version 2.0
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @copyright 2016 ETS2Routes Dev Team
 */
class Routing {
	private $dom;
	
	public function __construct($XMLFile = NULL) {
		if ( !is_null($XMLFile) ) {
			$this->open($XMLFile);
		}
	}
	
	/**
	 * Open XMLFile
	 * @param string $XMLFile
	 * @return boolean
	 */
	public function open($XMLFile) {
		if ( file_exists($XMLFile) ) {
			$dom = new DOMDocument();
			
			$return = $dom->load($XMLFile, LIBXML_DTDVALID);
			
			if ($return) {
				$this->dom = $dom;
				return true;
			}
		}
		
		return false;
	}
	
	public function getPath($routeName) {
		foreach ( $this->dom->getElementsByTagName('name') as $node ) {
			if ( $node->nodeValue === $routeName ) {
				return $node->nextSibling->nextSibling->nodeValue;
			}
		}
	}
	
	/**
	 * Search good route and call controller
	 * @param string $url
	 * @return string if controller has been find or false
	 */
	public function searchRoute($url) {
		$call = array();
		$explode_url = $this->explode($url);
		
		foreach ( $this->dom->getElementsByTagName('path') as $node ) {
			$exploding_path = $this->explode($node->nodeValue);
			
			if ( count($explode_url) === count($exploding_path) ) {
				$good = false;
				
				for ($i = 0; $i < count($explode_url); $i++) {
					$parameters = $this->parameters($exploding_path);
					
					if ( $explode_url[$i] === $exploding_path[$i] ) {
						$good = true;
					}
					else if ( substr($exploding_path[$i], 0, 1) === '{' &&
							  substr($exploding_path[$i], -1) === '}' ) {
						$call[] = $explode_url[$i];
						$good = true;
					}
					else {
						$good = false;
					}
				}
				
				if ( $good ) {
					return array(
						'call' => $node->parentNode->lastChild->previousSibling->nodeValue,
						'args' => $call
					);
				}
			}
		}
		
		return false;
	}
	
	/**
	 * Exploding string
	 * 
	 * @param string $string
	 * <div>The input string</div>
	 * 
	 * @return array
	 * <div>An array containing exploding string</dov>
	 */
	private function explode($string) {
		$array = array();
		
		$array[] = '/';
		
		$tok = strtok($string, '/');
		
		while ( $tok !== false ) {
			$array[] = $tok;
			$tok = strtok('/');
		}
		
		return $array;
	}
		
	/**
	 * Search parameters
	 * @param array $explode
	 * @return array list of parameters
	 */
	private function parameters($explode) {
		$array = array();
		
		foreach ($explode as $key =>$val) {
			if ( substr($val, 0, 1) === '{' && substr($val, -1) === '}' ) {
				$array[$key] = $val;
			}
		}
		
		return $array;
	}
	
	/**
	 * Call controller
	 * @param string $XMLController
	 */
	public function callController($XMLController, $args = array()) {
		$explode = explode(':', $XMLController);
		
		$ctr = $explode[0].'\\'.$explode[1].'Controller';
		$method = $explode[2];
		
		$controller = new $ctr();
		$controller->$method($args);
	}
}
