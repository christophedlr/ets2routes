<?php

require_once 'includes/consts.php';
require_once 'includes/autoloader.php';
if ( file_exists('includes/database.php') ) { require_once 'includes/database.php'; }

require_once LIBS_PATH.'/autoload.php';
require_once 'includes/symfony_routing.php';
//require_once LIBS_PATH.'/routing/routing.php';
require_once LIBS_PATH.'/formgen/Form.php';

//$modules = Core\ModulesController::getInstance();

try {
	$match = $matcher->match('/'.$_GET['url']);
	$args = array();
	
	$explode = explode(':', $match['_controller']);
	
	foreach ($match as $key => $val) {
		if ( substr($key, 0, 1) !== '_') {
			$args[$key] = $val;
		}
	}
	
	$ctr = $explode[0].'\\'.$explode[1].'Controller';
	$method = $explode[2].'Action';
	
	$controller = new $ctr();
	$controller->$method( (object)$args );
} catch (Exception $e) {
	echo $e->getMessage();
}
