-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           10.1.10-MariaDB - mariadb.org binary distribution
-- SE du serveur:                Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- Export de données de la table ets2routes.cities : ~0 rows (environ)
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` (`id_city`, `name`) VALUES
	(1, 'Aberdeen'),
	(2, 'Glasgow'),
	(3, 'Edinburgh'),
	(4, 'Newcastle-upon-Tyre'),
	(5, 'Carlisle'),
	(6, 'Liverpool'),
	(7, 'Manchester'),
	(8, 'Grimsby'),
	(9, 'Sheffield'),
	(10, 'Birmingham'),
	(11, 'Swansea'),
	(12, 'Cardiff'),
	(13, 'Plymouth'),
	(14, 'Cambridge'),
	(15, 'Felixstowe'),
	(16, 'London'),
	(17, 'Southampton'),
	(18, 'Dover'),
	(19, 'Calais'),
	(20, 'Paris'),
	(21, 'Lille'),
	(22, 'Reims'),
	(23, 'Brussel'),
	(24, 'Dijon'),
	(25, 'Metz'),
	(26, 'Lyon'),
	(27, 'Genève'),
	(28, 'Bern'),
	(29, 'Zürich'),
	(30, 'Strasbourg'),
	(31, 'Stuttgart'),
	(32, 'Mannheim'),
	(33, 'Luxembourg'),
	(34, 'Liège'),
	(35, 'Köln'),
	(36, 'Düsseldorf'),
	(37, 'Rotterdam'),
	(38, 'Amsterdam'),
	(39, 'Duisburg'),
	(40, 'Dortmund'),
	(41, 'Frankfurt am Main'),
	(42, 'Torino'),
	(43, 'Milano'),
	(44, 'Verona'),
	(45, 'Venezia'),
	(46, 'Klagenfurt am Wörthersee'),
	(47, 'Innsbruck'),
	(48, 'Graz'),
	(49, 'Salzburg'),
	(50, 'München'),
	(51, 'Nürnberg'),
	(52, 'Kassel'),
	(53, 'Groningen'),
	(54, 'Osnabrück'),
	(55, 'Hannover'),
	(56, 'Erfurt'),
	(57, 'Bremen'),
	(58, 'Magdeburg'),
	(59, 'Leipzig'),
	(60, 'Dresden'),
	(61, 'Wien'),
	(62, 'Bratislava'),
	(63, 'Pècs'),
	(64, 'Budapest'),
	(65, 'Szeged'),
	(66, 'Debreon'),
	(67, 'Koňce'),
	(68, 'Ranska Bystrica'),
	(69, 'Krakōw'),
	(70, 'Obsztyn'),
	(71, 'Gdańsk'),
	(72, 'Szczecim'),
	(73, 'Rostock'),
	(74, 'Hamburg'),
	(75, 'Kiel'),
	(76, 'Gedsea'),
	(77, 'Trelleborg'),
	(78, 'Gdense'),
	(79, 'Esberj'),
	(80, 'Københarn'),
	(81, 'Helsingborg'),
	(82, 'Karistrova'),
	(83, 'Aalborg'),
	(84, 'Viaxjö'),
	(85, 'Jönköping'),
	(86, 'Frederikshavn'),
	(87, 'Hirtshais'),
	(88, 'Göteborg'),
	(89, 'Linköping'),
	(90, 'Nynāishann'),
	(91, 'Sodertälje'),
	(92, 'Stockholm'),
	(93, 'Uppsala'),
	(94, 'Västeràs'),
	(95, 'Örebro'),
	(96, 'Oslo'),
	(97, 'Kristinannand'),
	(98, 'Staranger'),
	(99, 'Bergen'),
	(100, 'Berlin');
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           10.1.10-MariaDB - mariadb.org binary distribution
-- SE du serveur:                Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- Export de données de la table ets2routes.trucks_mark : ~7 rows (environ)
/*!40000 ALTER TABLE `trucks_mark` DISABLE KEYS */;
INSERT INTO `trucks_mark` (`id_mark`, `name`) VALUES
	(1, 'Volvo'),
	(2, 'Mercedes-Benz'),
	(3, 'Scania'),
	(4, 'Renault'),
	(5, 'DAF'),
	(6, 'Iveco'),
	(7, 'MAN');
/*!40000 ALTER TABLE `trucks_mark` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           10.1.10-MariaDB - mariadb.org binary distribution
-- SE du serveur:                Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- Export de données de la table ets2routes.trucks : ~0 rows (environ)
/*!40000 ALTER TABLE `trucks` DISABLE KEYS */;
INSERT INTO `trucks` (`id_truck`, `id_mark`, `name`) VALUES
	(80, 1, 'FH16 2009 (540 cv)'),
	(81, 1, 'FH16 2009 (600 cv)'),
	(82, 1, 'FH16 2009 (700 cv)'),
	(83, 1, 'FH16 2009 (750 cv)'),
	(84, 1, 'FH16 2012 (420 cv)'),
	(85, 1, 'FH16 2012 (460 cv)'),
	(86, 1, 'FH16 2012 (500 cv)'),
	(87, 1, 'FH16 2012 (540 cv)'),
	(88, 1, 'FH16 2012 (600 cv)'),
	(89, 1, 'FH16 2012 (700 cv)'),
	(90, 1, 'FH16 2012 (750 cv)'),
	(32, 2, 'Actros (320 cv)'),
	(33, 2, 'Actros (360 cv)'),
	(34, 2, 'Actros (408 cv)'),
	(35, 2, 'Actros (435 cv)'),
	(36, 2, 'Actros (456 cv)'),
	(37, 2, 'Actros (476 cv)'),
	(38, 2, 'Actros (510 cv)'),
	(39, 2, 'Actros (551 cv)'),
	(40, 2, 'Actros (598 cv)'),
	(41, 2, 'New Actros (421 cv)'),
	(42, 2, 'New Actros (449 cv)'),
	(43, 2, 'New Actros (476 cv)'),
	(44, 2, 'New Actros (510 cv)'),
	(45, 2, 'New Actros (517 cv)'),
	(46, 2, 'New Actros (578 cv)'),
	(47, 2, 'New Actros (625 cv)'),
	(54, 3, 'R360'),
	(55, 3, 'R380'),
	(56, 3, 'R400'),
	(57, 3, 'R420'),
	(58, 3, 'R440'),
	(59, 3, 'R480'),
	(60, 3, 'R500'),
	(61, 3, 'R560'),
	(62, 3, 'R620'),
	(63, 3, 'R730'),
	(64, 3, 'Streamline (360 cv)'),
	(66, 3, 'Streamline (370 cv)'),
	(65, 3, 'Streamline (380 cv)'),
	(67, 3, 'Streamline (400 cv)'),
	(70, 3, 'Streamline (410 cv)'),
	(68, 3, 'Streamline (420 cv)'),
	(69, 3, 'Streamline (440 cv)'),
	(71, 3, 'Streamline (450 cv)'),
	(72, 3, 'Streamline (480 cv)'),
	(73, 3, 'Streamline (490 cv)'),
	(74, 3, 'Streamline (500 cv)'),
	(77, 3, 'Streamline (520 cv)'),
	(75, 3, 'Streamline (560 cv)'),
	(78, 3, 'Streamline (580 cv)'),
	(76, 3, 'Streamline (620 cv)'),
	(79, 3, 'Streamline (730 cv)'),
	(48, 4, 'Magnum (440 cv)'),
	(49, 4, 'Magnum (480 cv)'),
	(50, 4, 'Magnum (520 cv)'),
	(51, 4, 'Premium (380 cv)'),
	(52, 4, 'Premium (430 cv)'),
	(53, 4, 'Premium (460 cv)'),
	(1, 5, 'XF 105 (360 cv)'),
	(2, 5, 'XF 105 (410 cv)'),
	(3, 5, 'XF 105 (460 cv)'),
	(4, 5, 'XF 105 (510 cv)'),
	(5, 5, 'XF Euro 6 (410 cv)'),
	(6, 5, 'XF Euro 6 (435 cv)'),
	(7, 5, 'XF Euro 6 (460 cv)'),
	(8, 5, 'XF Euro 6 (510 cv)'),
	(9, 6, 'Stralis (310 cv)'),
	(10, 6, 'Stralis (330 cv)'),
	(11, 6, 'Stralis (360 cv)'),
	(12, 6, 'Stralis (420 cv)'),
	(13, 6, 'Stralis (450 cv)'),
	(15, 6, 'Stralis (500 cv)'),
	(16, 6, 'Stralis (560 cv)'),
	(24, 6, 'Stralis Hi-Way (310 cv)'),
	(17, 6, 'Stralis Hi-Way (330 cv)'),
	(18, 6, 'Stralis Hi-Way (360 cv)'),
	(19, 6, 'Stralis Hi-Way (400 cv)'),
	(20, 6, 'Stralis Hi-Way (420 cv)'),
	(21, 6, 'Stralis Hi-Way (460 cv)'),
	(22, 6, 'Stralis Hi-Way (500 cv)'),
	(23, 6, 'Stralis Hi-Way (560 cv)'),
	(25, 7, 'TGX (320 cv)'),
	(26, 7, 'TGX (360 cv)'),
	(27, 7, 'TGX (400 cv)'),
	(28, 7, 'TGX (440 cv)'),
	(29, 7, 'TGX (480 cv)'),
	(30, 7, 'TGX (540 cv)'),
	(31, 7, 'TGX (680 cv)');
/*!40000 ALTER TABLE `trucks` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           10.1.10-MariaDB - mariadb.org binary distribution
-- SE du serveur:                Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- Export de données de la table ets2routes.trailers : ~0 rows (environ)
/*!40000 ALTER TABLE `trailers` DISABLE KEYS */;
INSERT INTO `trailers` (`id`, `name`) VALUES
	(1, 'Boeufs'),
	(2, 'Viande de poulet'),
	(3, 'Tripes d\'agneau'),
	(4, 'Viande de porc'),
	(5, 'Saucisses'),
	(6, 'Dynamite'),
	(7, 'Explosifs'),
	(8, 'Feux d\'artifice'),
	(9, 'Briques'),
	(10, 'Voitures'),
	(11, 'SUV de luxe'),
	(12, 'Ciment'),
	(13, 'Granulés plastiques'),
	(14, 'Béton'),
	(15, 'Acide'),
	(16, 'Produits chimiques'),
	(17, 'Produits chauds'),
	(18, 'Acide hydrochlorydrique'),
	(19, 'Merlu surgelé'),
	(20, 'Glace'),
	(21, 'Buses en béton'),
	(22, 'Réservoir pressurisé'),
	(23, 'Réservoir'),
	(24, 'Plateaux'),
	(25, 'Tube de ventilation'),
	(26, 'Lait'),
	(27, 'Filets de morue de l\'Atlantique'),
	(28, 'Boissons'),
	(29, 'Conserves de boeufs'),
	(30, 'Conserves de porcs'),
	(31, 'Gasoil'),
	(32, 'Hydrogène'),
	(33, 'Kérosène'),
	(34, 'Pétrole'),
	(35, 'Essence'),
	(36, 'LPG'),
	(37, 'Panneau de verre'),
	(38, 'Pommes'),
	(39, 'Big-bags de graines'),
	(40, 'Farine'),
	(41, 'Graisse graphitée'),
	(42, 'Liquides de freins'),
	(43, 'Conserves de thon'),
	(44, 'Absorbant chimique'),
	(45, 'Vètements'),
	(46, 'Arsenic'),
	(47, 'Plomb'),
	(48, 'Hydroxide de sodium'),
	(49, 'Acide sulfurique'),
	(50, 'Acétylène'),
	(51, 'Chlore'),
	(52, 'Matériel contaminé'),
	(53, 'Cyanure'),
	(54, 'Fluor'),
	(55, 'Métaux lourds'),
	(56, 'Déchets hospitaliers'),
	(57, 'Magnésium'),
	(58, 'Chlorure mercurique'),
	(59, 'Néon'),
	(60, 'Nitrocellulose'),
	(61, 'Azote'),
	(62, 'Pesticides'),
	(63, 'Phosphore blanc'),
	(64, 'Potassium'),
	(65, 'Sodium'),
	(66, 'Réservoir d\'essence'),
	(67, 'Bovins'),
	(68, 'Grumes'),
	(69, 'Bois de charpente'),
	(70, 'Canalisations'),
	(71, 'Chargeuse sur pneus'),
	(72, 'Rétrocaveuse'),
	(73, 'Tractopelles'),
	(74, 'Pelleteuse'),
	(75, 'Chariots élévateurs'),
	(76, 'Tracteurs'),
	(77, 'Nacelle d\'éolienne'),
	(78, 'Mât d\'éolienne'),
	(79, 'Plancher modulaire'),
	(80, 'Vitres'),
	(81, 'Murs préfabriqués'),
	(82, 'Amandes'),
	(83, 'Haricots'),
	(84, 'Conserves de haricots'),
	(85, 'Orge'),
	(86, 'Seigle'),
	(87, 'Ferraille'),
	(88, 'Sable de carrière'),
	(89, 'Blé'),
	(90, 'Ecorce de bois'),
	(91, 'Copeaux de bois'),
	(92, 'Charbon'),
	(93, 'Gravier'),
	(94, 'Minerai'),
	(95, 'Sable'),
	(96, 'Panneau d\'aggloméré'),
	(97, 'Rouleaux de film plastique'),
	(98, 'Beurre de cacahuète'),
	(99, 'Conduits de cheminée'),
	(100, 'Escaliers en préfabriqués'),
	(101, 'Jantes de camion'),
	(102, 'Palettes vides'),
	(103, 'Batteries de camion'),
	(104, 'Lanterneaux'),
	(105, 'Pots de fleurs');
/*!40000 ALTER TABLE `trailers` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           10.1.10-MariaDB - mariadb.org binary distribution
-- SE du serveur:                Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- Export de données de la table ets2routes.faq : ~9 rows (environ)
/*!40000 ALTER TABLE `faq` DISABLE KEYS */;
INSERT INTO `faq` (`id_faq`, `question`, `response`) VALUES
	(1, 'Qu\'est ce que ETS2Routes ?', 'ETS2Routes est un site internet consacré au jeu Euro Truck Simulator 2 de SCS Software.'),
	(2, 'Pourquoi ce site ?', 'Le jeu Euro Truck Simulator 2 propose peut de gestion d\'entreprises. On recrute certes des PNJ qui vont rouler (virtuellement car on ne les vois hélas pas) et nous apporter de l\'argent, mais ça s\'arrête là.\r\n\r\nLe principe d\'ETS2Routes, est d\'avoir comme employés, des joueurs bien réels réunis autour d\'une entreprise. Chacun y note les trajets qu\'il fait, ce qui permet de garder un historique détaillé de tout ce qui est fait sur la route, que se soit un historique des gains, de l\'XP etc.'),
	(3, 'Comment ça marche ?', 'L\'utilisation est simple, dans le menu du haut, sans avoir de compte vous pouvez voir la FAQ et nous contacter.\r\n\r\nL\'inscription est gratuite et ne prend que quelques minutes, une fois que c\'est fait vous avez un lien "Entreprises". En cliquant dessus vous pouvez candidater dans une entreprise ou en créer une.\r\n\r\nQuelque soit votre choix, une fois que vous êtes dans une entreprise, vous pouvez inscrire vos rapports de trajets.'),
	(4, 'Comment fonctionne les entreprises ?', 'En créant une entreprise, vous pouvez modifier quelques options comme le pourcentage de gain par trajets.'),
	(5, 'Le site est-il complet ?', 'Non, actuellement la version que vous avez sous les yeux est une bêta.\r\nUne refonte total est prévue, cette dernière sera la version "finale" (on parle de Release en informatique).\r\n\r\nCependant, vous pouvez tout à fait utiliser la version actuelle, elle est fonctionnelle mais manquent de quelques fonctionnalités prévues au départ.'),
	(6, 'ETS2routes est un simple site internet sur le jeu ETS2 ?', 'Non, ETS2Routes est une "application web". Vous pouvez retrouver la version actuelle sur le dépôt bitbucket.org (cherchez ETS2Routes et vous trouverez).\r\n\r\nLa version finale, sera présente sur github.com.\r\nL\'application est entièrement gratuite et sous la licence CeCILL V2 (licence libre).\r\nVous pouvez donc modifier l\'application comme bon vous semble, l\'utiliser pour vous même (sur votre propre serveur) etc.\r\n\r\nMême si l\'intérêt premier de ETS2Routes, est d\'être exploité par tous sur son site officiel : ets2routes.com, le fait est que vous êtes libre de faire votre propre installation de l\'application et d\'en fournir l\'accès.'),
	(7, 'Toutes les villes sont présentes ?', 'Oui, toutes les villes proposées par le contenu de base (+ ses deux DLC) du jeu, sont présentes.\r\n\r\nIl peut toutefois arriver qu\'une ai été oubliée, devant la quantité il serait logique de faire un oubli. Dans ce cas là, nous vous invitons à nous contacter afin que nous puissions rajouter ce qui manque.'),
	(8, 'Comme pour les villes, les remorques, marques de camions et camions sont présents ?', 'Oui, toutes les données fournies par le jeu sont présentes. Dans les remorques, certaines manquent certainement car la liste donnée par le jeu n\'est pas complète (dans son menu A propos > voir les remorques), des  remorques ont certainement été oubliés.'),
	(9, 'Dans la liste des camions, un nombre de cv (chevaux) est indiqués, c\'est quoi ?', 'Habituellement sur ETS2, on définit par les camions de la marque Scania, par un numéro : Scania R360 par exemple, il s\'agît de la motorisation. Ainsi un Scani R360 correspond au modèle R de chez Scania avec un moteur de 360 chevaux (360 cv).\r\n\r\ndu coup cela fait beaucoup de camions pour Scania, mais sur les autres camions on a pas de quoi spécifier directement la motorisation, ainsi le nombre de chevaux indiqués entre parenthèse correspond à la motorisation.\r\n\r\nCela fait une liste assez grande, mais permet à chacun de retrouver sa marque de camion, son modèle et la motorisation de son camion, de quoi se distinguer des collègues.');
/*!40000 ALTER TABLE `faq` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
