<?php

namespace Core;

use Core\AppModels;

/**
 * Employees of enterprises models
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @author Christophe Daloz - De Los Rios <christophedlr@gmail.com>
 * @copyright ETS2Routes Dev Team 2016
 */
class EntEmployeesModels extends AppModels {
	public function getEmployee($id_users) {
		$db = $this->db;
		$statement = 'SELECT * FROM `ent_employees` WHERE `id_users` = :id_users';
		
		$query = $this->db->prepare($statement);
		$query->bindParam(':id_users', $id_users, $db::PARAM_INT);
		
		if ( $query->execute() ) {
			return $query->fetch($db::FETCH_ASSOC);
		}
		
		return array();
	}
	
	public function getAllEmployees() {
		$db = $this->db;
		$statement = 'SELECT * FROM `ent_employees`';
		
		$query = $this->db->query($statement);
		
		if ( $query->execute() ) {
			return $query->fetchAll($db::FETCH_ASSOC);
		}
		
		return array();
	}
	
	public function addEmployees($data) {
		$db = $this->db;
		$statement = 'INSERT INTO `ent_employees` VALUES(:id_ent, :id_users, :access, :candidacy)';
		
		$query = $this->db->prepare($statement);
		$query->bindParam(':id_ent', $data['id_ent'], $db::PARAM_INT);
		$query->bindParam(':id_users', $data['id_users'], $db::PARAM_INT);
		$query->bindParam(':candidacy', $data['candidacy']);
		
		if ( isset($data['access']) ) {
			$query->bindParam(':access', $data['access'], $db::PARAM_INT);
		}
		else {
			$access = 1;
			$query->bindParam(':access', $access, $db::PARAM_INT);
		}
		
		return $query->execute();
	}
	
	public function changeEmployee($id, $access) {
		$statement = 'UPDATE `ent_employees` SET `access` = '.$access.' WHERE `id_users` = '.$id;
		
		$query = $this->db->query($statement);
		
		return $query->execute();
	}
	
	public function delEmployee($id_ent, $id_users) {
		$statement = 'DELETE FROM `ent_employees` WHERE `id_ent` = '.$id_ent.' AND `id_users` = '.$id_users;
		
		$query = $this->db->query($statement);
		
		return $query->execute();
	}
	
	public function delAllEmployee($id_ent) {
		$statement = 'DELETE FROM `ent_employees` WHERE `id_ent` = '.$id_ent;
	
		$query = $this->db->query($statement);
	
		return $query->execute();
	}
	
	public function candidacyAccepted($id_users) {
		$statement = 'UPDATE `ent_employees` SET `access` = 1 WHERE `id_users` = '.$id_users;
		
		$query = $this->db->query($statement);
		
		return $query->execute();
	}
	
	public function candidacyRejected($id_users) {
		$statement = 'DELETE FROM `ent_employees` WHERE `id_users` = '.$id_users;
	
		$query = $this->db->query($statement);
	
		return $query->execute();
	}
}
