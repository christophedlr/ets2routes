<?php

namespace Core;

use Core\AppModels;

/**
 * Manage users table
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @author Christophe Daloz - De Los Rios <christophedlr@gmail.com>
 * @copyright ETS2Routes Dev Team 2016
 */
class UsersModels extends AppModels {
	/**
	 * Select user informations in database
	 * @param integer $id
	 * <div>ID of user</div>
	 * 
	 * @return array
	 * <div>List of informations of user</div>
	 */
	public function getUser($id) {
		$db = $this->db;
		$statement = 'SELECT * FROM `users` WHERE `id_users` = :id';
		
		$query = $this->db->prepare($statement);
		$query->bindParam(':id', $id, $db::PARAM_INT);
		
		if ( $query->execute() ) {
			return $query->fetch($db::FETCH_OBJ);
		}
	}
	
	/**
	 * Select user informations by login in database
	 * @param string $login
	 * <div>Login of user</div>
	 *
	 * @return array
	 * <div>List of informations of user</div>
	 */
	public function getUserByLogin($login) {
		$db = $this->db;
		$statement = 'SELECT * FROM `users` WHERE `login` = :login';
	
		$query = $this->db->prepare($statement);
		$query->bindParam(':login', $login);
	
		if ( $query->execute() ) {
			return $query->fetch($db::FETCH_OBJ);
		}
	}
	
	/**
	 * SElect all users
	 * 
	 * @return array
	 * <div>List of users</div>
	 */
	public function getAllUsers() {
		$db = $this->db;
		$statement = 'SELECT * FROM `users`';
		
		$query = $this->db->query($statement);
		
		if ($query->execute()) {
			return $query->fetchAll($db::FETCH_ASSOC);
		}
	}
	
	/**
	 * Register new user in database
	 * @param array $userArray
	 * <div>List of form fields</div>
	 * 
	 * @return boolean
	 * <div>TRUE if user has been register or FALSE if not</div>
	 */
	public function setUser($userArray) {
		$db = $this->db;
		$statement = 'INSERT INTO `users` (`access`, `login`, `password`, `mail`, `registerDate`)
			VALUES(:access, :login, :password, :mail, NOW())';
		
		$query = $this->db->prepare($statement);
		$hash = hash('sha256', $userArray['password']);
		$access = 0x02;
		
		if ( isset($userArray['access']) ) {
			$access = intval($userArray['access']);
		}
		
		$query->bindParam(':access', $access, $db::PARAM_INT);
		$query->bindParam(':login', $userArray['login']);
		$query->bindParam(':password', $hash );
		$query->bindParam(':mail', $userArray['mail']);
		
		return $query->execute();
	}
	
	public function setEnterprise($id_enterprise, $id, $access = 0x02) {
		$statement = 'UPDATE `users` SET `id_enterprise` = '.$id_enterprise.',
			`access_ent` = '.$access.' WHERE `id_users` = '.$id;
		$query = $this->db->query($statement);
		
		return $query->execute();
	}
	
	/**
	 * Change user in database
	 * @param array $userArray
	 * <div>List of form fields</div>
	 *
	 * @return boolean
	 * <div>TRUE if user has been register or FALSE if not</div>
	 */
	public function changeUser($userArray) {
		$db = $this->db;
		$password = '`password`= :password';
		$pass = false;
		
		if ( !empty($userArray['password']) && $userArray['password'] === $userArray['conf-password']) {
			$statement = 'UPDATE `users` SET `login`= :login, `mail` = :mail, `access`= :access, '.$password.' WHERE `id_users` = :id';
			$pass = true;
		}
		else {
			$statement = 'UPDATE `users` SET `login`= :login, `mail` = :mail, `access`= :access WHERE `id_users` = :id';
			$pass = false;
		}
	
		$query = $this->db->prepare($statement);
		$hash = hash('sha256', $userArray['password']);
		$access = intval($userArray['access']);
		$id = intval($userArray['id']);
		
		if ($pass) {
			$query->bindParam(':password', $hash );
		}
	
		$query->bindParam(':id', $id, $db::PARAM_INT);
		$query->bindParam(':access', $access, $db::PARAM_INT);
		$query->bindParam(':login', $userArray['login']);
		$query->bindParam(':mail', $userArray['mail']);
	
		return $query->execute();
	}
	
	/**
	 * Delete the selected user
	 * @param integer $id
	 * <div>ID of selected user</div>
	 * 
	 * @return boolean
	 * <div>TRUE if delete user or fALSE if error</div>
	 */
	public function deleteUser($id) {
		$db = $this->db;
		$statement = 'DELETE FROM `users` WHERE `id_users` = :id';
		
		$query = $this->db->prepare($statement);
		
		$query->bindParam(':id', $id, $db::PARAM_INT);
		
		return $query->execute();
	}
}
