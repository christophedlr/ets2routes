<?php

namespace Core;

use Core\AppModels;

/**
 * Enterprises models
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @author Christophe Daloz - De Los Rios <christophedlr@gmail.com>
 * @copyright ETS2Routes Dev Team 2016
 */
class EnterprisesModels extends AppModels {
	public function getEnterprise($id) {
		$db = $this->db;
		$statement = 'SELECT * FROM `enterprises` WHERE `id_enterprise` = :id';
		
		$query = $this->db->prepare($statement);
		$query->bindParam(':id', $id, $db::PARAM_INT);
		
		if ( $query->execute() ) {
			return $query->fetch($db::FETCH_ASSOC);
		}
		else {
			return array();
		}
	}
	
	public function getAllEnterprises() {
		$db = $this->db;
		$statement = 'SELECT * FROM `enterprises`';
		
		$query = $this->db->query($statement);
		
		if ( $query->execute() ) {
			return $query->fetchAll($db::FETCH_ASSOC);
		}
		else {
			return array();
		}
	}
	
	public function addEnterprise($data, &$id = NULL) {
		$db = $this->db;
		$statement = 'INSERT INTO `enterprises` VALUES("", :tag, :name, NULL, :gain_path,
			:spend_impact, :base_salary)';
		
		$query = $this->db->prepare($statement);
		$query->bindParam(':tag', $data['tag'], $db::PARAM_INT);
		$query->bindParam(':name', $data['name']);
		$query->bindParam(':gain_path', $data['gain_path'], $db::PARAM_INT);
		$query->bindParam(':spend_impact', $data['spend_impact'], $db::PARAM_INT);
		$query->bindParam(':base_salary', $data['base_salary'], $db::PARAM_INT);
		
		if ( $query->execute() ) {
			$id = $this->db->lastInsertId();
			
			return true;
		}
		
		return false;
	}
	
	public function changeEnterprise($data) {
		$db = $this->db;
		$statement = 'UPDATE `enterprises` SET `gain_path` = :gain_path,
			`spend_impact` = :spend_impact, `base_salary` = :base_salary WHERE `id_enterprise` = :id';
		
		$query = $this->db->prepare($statement);
		$query->bindParam(':id', $data['id'], $db::PARAM_INT);
		$query->bindParam(':gain_path', $data['gain_path'], $db::PARAM_INT);
		$query->bindParam(':spend_impact', $data['spend_impact'], $db::PARAM_INT);
		$query->bindParam(':base_salary', $data['base_salary'], $db::PARAM_INT);
		
		return $query->execute();
	}
	
	public function delEnterprise($id_enterprise) {
		$db = $this->db;
		$statement = 'DELETE FROM `enterprises` WHERE `id_enterprise` = :id_enterprise';
		
		$query = $this->db->prepare($statement);
		$query->bindParam(':id_enterprise', $id_enterprise, $db::PARAM_INT);
		
		return $query->execute();
	}
}
