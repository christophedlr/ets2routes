<?php

namespace Core;

/**
 * Application model
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @author Christophe Daloz - De Los Rios <christophedlr@gmail.com>
 * @copyright ETS2Routes Dev Team 2016
 */
class AppModels {
	protected $db;
	
	public function __construct() {
		global $database;
		
		$this->db = new \PDO($database['dsn'], $database['user'], $database['password']);
		$this->db->exec("SET CHARACTER SET utf8");
	}
}
