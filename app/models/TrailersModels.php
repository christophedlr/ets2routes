<?php

namespace Core;

use Core\AppModels;

/**
 * Trailers models
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @author Christophe Daloz - De Los Rios <christophedlr@gmail.com>
 * @copyright ETS2Routes Dev Team 2016
 */
class TrailersModels extends AppModels {
	public function getTrailers() {
		$db = $this->db;
		$statement = 'SELECT * FROM `trailers` ORDER BY `name`';
		$query = $this->db->query($statement);
		
		if ( $query->execute() ) {
			return $query->fetchAll($db::FETCH_OBJ);
		}
		else {
			return array();
		}
	}
	
	public function addTrailer($name) {
		$statement = 'INSERT INTO `trailers` VALUES(NULL, :name)';
		
		$query = $this->db->prepare($statement);
		//$query->bindParam(':weight', $weight, $db::PARAM_INT);
		$query->bindParam(':name', $name);
		
		return $query->execute();
	}
	
	public function delTrailer($id) {
		$db = $this->db;
		$statement = 'DELETE FROM `trailers` WHERE `id` = :id';
		
		$query = $this->db->prepare($statement);
		$query->bindParam(':id', $id, $db::PARAM_INT);
		
		return $query->execute();
	}
}
