<?php

namespace Core;

use Core\AppModels;

/**
 * Cities models
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @author Christophe Daloz - De Los Rios <christophedlr@gmail.com>
 * @copyright ETS2Routes Dev Team 2016
 */
class CitiesModels extends AppModels {
	public function getCities() {
		$db = $this->db;
		$statement = 'SELECT * FROM `cities` ORDER BY `name`';
		
		$query = $this->db->query($statement);
		if ( $query->execute() ) {
			return $query->fetchAll($db::FETCH_OBJ);
		}
		else {
			return array();
		}
	}
	
	public function addCity($name) {
		$db = $this->db;
		$statement = 'INSERT INTO `cities` VALUES(NULL, :name)';
		
		$query = $this->db->prepare($statement);
		$query->bindParam(':name', $name);
		
		return $query->execute();
	}
	
	public function delCity($name) {
		$db = $this->db;
		$statement = 'DELETE FROM `cities` WHERE `name` = :name';
		
		$query = $this->db->prepare($statement);
		$query->bindParam(':name', $name);
		
		return $query->execute();
	}
	
	public function delCityWithID($id) {
		$db = $this->db;
		$statement = 'DELETE FROM `cities` WHERE `id_city` = :id';
	
		$query = $this->db->prepare($statement);
		$query->bindParam(':id', $id, $db::PARAM_INT);
	
		return $query->execute();
	}
}
