<?php

namespace Core;

use Core\AppModels;

class ConfigModels extends AppModels {
	public function save($post) {
		if ( $post->database === 'mysql' && !empty($post->dbname) && !empty($post->host) &&
			!empty($post->user) && !empty($post->password) ) {
				$database = array(
					'dsn' => '\''.$post->database.':dbname='.$post->dbname.'; host='.$post->host.'\'',
					'user' => '\''.$post->user.'\'',
					'password' => '\''.$post->password.'\''
			);
				
			$string = '<?php'.PHP_EOL.PHP_EOL;
				
			foreach ($database as $key => $val) {
				$string .= '$database[\''.$key.'\'] = '.$val.';'.PHP_EOL;
			}
				
			if ( !file_put_contents(LOCAL_PATH.'/includes/database.php', $string) ) {
				return array('type' => 'danger', 'msg' => 'La configuration n\'a pu être sauvegardée');
			}
			else {
				return array('type' => 'success', 'msg' => 'La configuration a bien été sauvegardée');
			}
		}
		else {
			return array('type' => 'danger', 'msg' => 'Vous devez compléter tout les champs du formulaire');
			}
	}
	
	public function load() {
		$string = file_get_contents(LOCAL_PATH.'/includes/database.php');
		$string = str_replace('<?php'.PHP_EOL.PHP_EOL, '', $string);
		eval($string);
		
		return $database;
	}
}
