<?php

namespace Core;

use Core\AppModels;

/**
 * Trucks mark models
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @author Christophe Daloz - De Los Rios <christophedlr@gmail.com>
 * @copyright ETS2Routes Dev Team 2016
 */
class TrucksMarkModels extends AppModels {
	public function getMarks() {
		$db = $this->db;
		$statement = 'SELECT * FROM `trucks_mark` ORDER BY `name';
		$query = $this->db->query($statement);
		
		if ( $query->execute() ) {
			return $query->fetchAll($db::FETCH_OBJ);
		}
		else {
			return array();
		}
	}
	
	public function getMarkWithName($name) {
		$db = $this->db;
		$statement = 'SELECT * FROM `trucks_mark` WHERE `name` = :name';
		
		$query = $this->db->prepare($statement);
		$query->bindParam(':name', $name);
		
		if ( $query->execute() ) {
			return $query->fetch($db::FETCH_OBJ);
		}
		
		return array();
	}
	
	public function getMark($id) {
		$db = $this->db;
		$statement = 'SELECT * FROM `trucks_mark` WHERE `id` = :id';
		
		$query = $this->db->prepare($statement);
		$query->bindParam(':id', $id, $db::PARAM_INT);
		
		if ( $query->execute() ) {
			return $query->fetch($db::FETCH_OBJ);
		}
		
		return array();
	}
	
	public function addMark($name) {
		$statement = 'INSERT INTO `trucks_mark` VALUES(NULL, :name)';
		
		$query = $this->db->prepare($statement);
		$query->bindParam(':name', $name);
		
		return $query->execute();
	}
	
	public function delMark($id) {
		$statement = 'DELETE FROM `trucks_mark` WHERE `id_mark` = :id';
		
		$query = $this->db->prepare($statement);
		$query->bindParam(':id', $id, $db::PARAM_INT);
		
		return $query->execute();
	}
}
