<?php

namespace Core;

use Core\AppModels;

/**
 * Trucks models
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @author Christophe Daloz - De Los Rios <christophedlr@gmail.com>
 * @copyright ETS2Routes Dev Team 2016
 */
class TrucksModels extends AppModels {
	public function getTrucks() {
		$db = $this->db;
		$statement = 'SELECT * FROM `trucks` ORDER BY `name`';
		$query = $this->db->query($statement);
		
		if ( $query->execute() ) {
			return $query->fetchAll($db::FETCH_OBJ);
		}
		else {
			return array();
		}
	}
	
	public function getTrucksWithName($name) {
		$db = $this->db;
		$statement = 'SELECT * FROM `trucks` WHERE `name` = :name';
		
		$query = $this->db->prepare($statement);
		$query->bindParam(':name', $name);
		
		if ( $query->execute() ) {
			return $query->fetch($db::FETCH_OBJ);
		}
		
		return array();
	}
	
	public function getTruck($id) {
		$db = $this->db;
		$statement = 'SELECT * FROM `trucks` WHERE `id_truck` = :id';
		
		$query = $this->db->prepare($statement);
		$query->bindParam(':id', $id, $db::PARAM_INT);
		
		if ( $query->execute() ) {
			return $query->fetch($db::FETCH_OBJ);
		}
		
		return array();
	}
	
	public function getTruckWithMark($id) {
		$db = $this->db;
		$statement = 'SELECT * FROM `trucks` WHERE `id_mark` = :id';
	
		$query = $this->db->prepare($statement);
		$query->bindParam(':id', $id, $db::PARAM_INT);
	
		if ( $query->execute() ) {
			return $query->fetch($db::FETCH_OBJ);
		}
	
		return array();
	}
	
	public function addTruck($id_mark, $name) {
		$db = $this->db;
		$statement = 'INSERT INTO `trucks` VALUES(NULL, :id_mark, :name)';
		
		$query = $this->db->prepare($statement);
		$query->bindParam(':id_mark', $id_mark, $db::PARAM_INT);
		$query->bindParam(':name', $name);
		
		return $query->execute();
	}
	
	public function delTruck($id) {
		$db = $this->db;
		$statement = 'DELETE FROM `trucks` WHERE `id_truck` = :id';
		
		$query = $this->db->prepare($statement);
		$query->bindParam(':id', $id, $db::PARAM_INT);
		
		return $query->execute();
	}
}
