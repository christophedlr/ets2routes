<?php

namespace Core;

/**
 * Modules models
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @author Christophe Daloz - De Los Rios <christophedlr@gmail.com>
 * @copyright ETS2Routes Dev Team 2016
 * @uses Core\AppModels Application principal model
 */
class ModulesModels extends AppModels {
	private $activateModules = array();
	
	public function __construct() {
		parent::__construct();
		$array = $this->getModulesList();
		if ( !empty($array) ) {
			foreach ($array as $key => $val) {
				if ( $val['status'] === '1' ) {
					$this->activateModules[] = $val;
				}
			}
		}
	}
	
	/**
	 * Get list of installed modules
	 * 
	 * @return array
	 * <div>Array list of installed modules or empty array</div> 
	 */
	public function getModulesList() {
		$query = $this->db->query('SELECT * FROM `modules`');
		if ( $query->execute() ) {
			return $query->fetchAll($this->db::FETCH_ASSOC);
		}
		
		return array();
	}
	
	/**
	 * Get list of activates modules
	 * 
	 * @return array
	 * <div>Array list of activates modules</div>
	 */
	public function getActivatesModules() {
		return $this->activateModules;
	}
}
