<?php

namespace Core;

use Core\AppModels;

/**
 * Report paths models
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @author Christophe Daloz - De Los Rios <christophedlr@gmail.com>
 * @copyright ETS2Routes Dev Team 2016
 */
class ReportPathsModels extends AppModels {
	public function getAllPaths() {
		$db = $this->db;
		$statement = 'SELECT * FROM `report_paths`';
		$query = $this->db->prepare($statement);
		
		if ( $query->execute() ) {
			return $query->fetchAll($db::FETCH_OBJ);
		}
		
		return array();
	}
	
	public function getPaths($id_ent) {
		$db = $this->db;
		$statement = 'SELECT * FROM `report_paths` WHERE `id_ent` = :id_ent';
		$query = $db->prepare($statement);
		$query->bindParam(':id_ent', $id_ent, $db::PARAM_INT);
	
		if ( $query->execute() ) {
			return $query->fetchAll($db::FETCH_OBJ);
		}
	
		return array();
	}
	
	public function addPath($data) {
		$db = $this->db;
		$statement = 'INSERT INTO `report_paths` VALUES(NULL, :id_user, :id_ent, :city_start, :city_end,
			:truck_mark, :truck_model, :profit, :traveled_dist, :xp, :cargo, :fuel, :comments, NOW())';
		
		$query = $this->db->prepare($statement);
		$query->bindParam(':id_user', $data['id_user'], $db::PARAM_INT);
		$query->bindParam(':id_ent', $data['id_ent'], $db::PARAM_INT);
		$query->bindParam(':city_start', $data['city-start'], $db::PARAM_INT);
		$query->bindParam(':city_end', $data['city-end'], $db::PARAM_INT);
		$query->bindParam(':truck_mark', $data['truck-mark'], $db::PARAM_INT);
		$query->bindParam(':truck_model', $data['truck-model'], $db::PARAM_INT);
		$query->bindParam(':profit', $data['profit'], $db::PARAM_INT);
		$query->bindParam(':traveled_dist', $data['traveled_dist'], $db::PARAM_INT);
		$query->bindParam(':xp', $data['xp'], $db::PARAM_INT);
		$query->bindParam(':fuel', $data['fuel'], $db::PARAM_INT);
		$query->bindParam(':cargo', $data['cargo'], $db::PARAM_INT);
		$query->bindParam(':comments', $data['comments']);
		
		return $query->execute();
	}
}
