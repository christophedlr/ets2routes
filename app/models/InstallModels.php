<?php

namespace Core;

use Core\AppModels;

class InstallModels extends AppModels {
	public function createTables() {
		$db = $this->db;
		$statement = file_get_contents(ASSETS_PATH.'/sql/create.sql');
		
		$result = $db->exec($statement);
		
		if ( $result === false ) {
			return false;
		}
		
		return true;
	}
	
	public function insertDatas() {
		$db = $this->db;
		$statement = file_get_contents(ASSETS_PATH.'/sql/insert.sql');
	
		$result = $db->exec($statement);
	
		if ( $result === false ) {
			return false;
		}
	
		return true;
	}
	
	public function dropTables() {
		$db = $this->db;
		$statement = file_get_contents(ASSETS_PATH.'/sql/drop.sql');
	
		$result = $db->exec($statement);
	
		if ( $result === false ) {
			return false;
		}
	
		return true;
	}
}
