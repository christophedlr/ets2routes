<?php

namespace Core\Admin;

use Core\AppController;
use Core\UsersModels;
use Modules\Faq\FaqController;
use Core\CitiesModels;
use Core\TrailersModels;
use Core\TrucksMarkModels;
use Core\TrucksModels;
use Core\ConfigModels;

/**
 * Administration controller
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @author Christophe Daloz - De Los Rios <christophedlr@gmail.com>
 * @copyright ETS2Routes Dev Team 2016
 */
class AdminController extends AppController {
	public function __construct() {
		parent::__construct();
		
		if ( intval($this->users->getUser()->access) < 16 ) {
			$this->redirect('index');
			exit;
		}
		
		$this->twig->addTwigBaseDir('app/admin', 'Admin');
	}
	
	public function indexAction($args) {
		$message = array();
		$config = new ConfigModels();
		
		if ( $args->type !== NULL ) {
			if ( $args->type === 'site' ) {
				if ( !empty($_POST) ) {
					$message = $config->save( (object)$_POST );
				}
				
				$array = $config->load();
				
				preg_match('#([^:]+):dbname=([^;]+);\s*host=(.+)#', $array['dsn'], $matches);
				
				$database = array(
					'database' => $matches[1],
					'dbname' => $matches[2],
					'host' => $matches[3],
					'user' => $array['user'],
					'password' => $array['password']
				);
				
				echo $this->twigRender('@Admin/config.twig', array(
						'message' => $message,
						'database' => $database
				));
			}
			else if ( $args->type === 'user' ) {
				$this->user($args->page);
			}
			else if ($args->type === 'faq') {
				$faq = new FaqController();
				$faq->adminAction($args);
			}
			else if ( $args->type === 'cities' ) {
				$this->cities($args->page);
			}
			else if ( $args->type === 'trailers' ) {
				$this->trailers($args->page);
			}
			else if ( $args->type === 'truckmark' ) {
				$this->truckMark($args->page);
			}
			else if ( $args->type === 'truck' ) {
				$this->trucks($args->page);
			}
		}
		else {
			echo $this->twigRender('@Admin/index.twig', array());
		}
	}
	
	private function user($page) {
		$message = array();
		if ( $page === 'list' ) {
			$this->userList();
		}
		else if ( substr( $page, 0, strpos($page, '-') ) === 'edit' ) {
			$this->editUser( intval( substr( $page, strpos($page, '-')+1 ) ) );
		}
		else if ( substr( $page, 0, strpos($page, '-') ) === 'del' ) {
			$usersModels = new UsersModels();
			$id = intval( substr( $page, strpos($page, '-')+1 ) );
				
			$result = $usersModels->deleteUser($id);
				
			if ( $result ) {
				$message['type'] = 'success';
				$message['msg'] = 'L\'utilisateur numéro <b>'.$id.'</b> a bien été supprimé';
			}
			else {
				$message['type'] = 'danger';
				$message['msg'] = 'Impossible de supprimer l\'utilisateur numéro <b>'.$id.'</b>';
			}
				
			$this->users->addTempVar('message', $message);
			$this->redirect(array('admin', 'user', 'list'));
		}
		else if ($page === 'add') {
			if ( !empty($_POST) ) {
				if ( !empty($_POST['login']) && !empty($_POST['mail']) &&
						!empty($_POST['password']) && !empty($_POST['access']) ) {
							$usersModels = new UsersModels();
							$result = $usersModels->setUser($_POST);
								
							if ( $result ) {
								$message['type'] = 'success';
								$message['msg'] = 'L\'utilisateur <b>'.$_POST['login'].'</b> a bien été enregistré';
							}
							else {
								$message['type'] = 'info';
								$message['msg'] = 'L\'utilisatuer n\'a pu être enregistrée. Veuillez retenter plus tard';
							}
						}
						else {
							$message['type'] = 'danger';
							$message['msg'] = 'Vous devez remplir tout les champs du formulaire';
						}
			}
				
			echo $this->twigRender('@Admin/users/add.twig', array(
					'message' => $message
			));
		}
	}
	
	private function cities($page) {
		$citiesModels = new CitiesModels();
		$message = array();
		$name = '';
		
		if ( $page === 'add' ) {
			if ( !empty($_POST) ) {
				if ( !empty($_POST['name']) ) {
					if ( $citiesModels->addCity($_POST['name']) ) {
						$message['type'] = 'success';
						$message['msg'] = 'La ville a bien été ajoutée';
		
						$this->users->addTempVar('message', $message);
						$this->redirect( array('admin', 'cities') );
						exit;
					}
					else {
						$message['type'] = 'danger';
						$message['msg'] = 'La ville n\'a pu être créer. Veuillez retenter.';
						$name = $_POST['name'];
					}
				}
				else {
					$message['type'] = 'danger';
					$message['msg'] = 'Vous indiquez un nom pour la ville';
				}
			}
				
			echo $this->twigRender('@Admin/cities_add.twig', array(
					'message' => $message,
					'name' => $name
			));
		}
		else if ( substr($page, 0, 3) === 'del' ) {
			$id = substr($page, 4);
				
			if ( $citiesModels->delCityWithID($id) ) {
				$message['type'] = 'success';
				$message['msg'] = 'La ville a bien été supprimée';
		
				$this->users->addTempVar('message', $message);
				$this->redirect( array('admin', 'cities') );
				exit;
			}
			else {
				$message['type'] = 'danger';
				$message['msg'] = 'Impossible de supprimer la ville';
				$this->redirect( array('admin', 'cities') );
				exit;
			}
		}
		else {
			echo $this->twigRender('@Admin/cities_list.twig', array(
					'cities' => $citiesModels->getCities(),
					'message' => $message
			));
		}
	}
	
	private function trailers($page) {
		$trailersModels = new TrailersModels();
		$message = array();
		$name = '';
		$weight = 0;
			
		if ( $page === 'add' ) {
			if ( !empty($_POST) ) {
				if ( !empty($_POST['name']) ) {
					if ( $trailersModels->addTrailer($_POST['name']/*, $_POST['weight']*/) ) {
						$message['type'] = 'success';
						$message['msg'] = 'La remorque a bien été ajoutée';
							
						$this->users->addTempVar('message', $message);
						$this->redirect( array('admin', 'trailers') );
						return;
					}
					else {
						$message['type'] = 'danger';
						$message['msg'] = 'La remorque n\'a pu être créer. Veuillez retenter.';
						$name = $_POST['name'];
						/*$weight = $_POST['wieght'];*/
					}
				}
				else {
					$message['type'] = 'danger';
					$message['msg'] = 'Vous indiquez un nom pour la remorque et un poids';
				}
			}
		
			echo $this->twigRender('@Admin/trailers_add.twig', array(
					'message' => $message,
					'name' => $name,
					//'weight' => $weight
			));
		}
		else if ( substr($page, 0, 3) === 'del' ) {
			$id = substr($page, 4);
		
			if ( $trailersModels->delTrailer($id) ) {
				$message['type'] = 'success';
				$message['msg'] = 'La remorque a bien été supprimée';
					
				$this->users->addTempVar('message', $message);
				$this->redirect( array('admin', 'trailers') );
				return;
			}
			else {
				$message['type'] = 'danger';
				$message['msg'] = 'Impossible de supprimer la remorque';
				$this->redirect( array('admin', 'trailers') );
				return;
			}
		}
		else {
			echo $this->twigRender('@Admin/trailers_list.twig', array(
					'trailers' => $trailersModels->getTrailers(),
					'message' => $message
			));
		}
	}
	
	private function truckMark($page) {
		$marksModels = new TrucksMarkModels();
		$message = array();
		$name = '';
			
		if ( $page === 'add' ) {
			if ( !empty($_POST) ) {
				if ( !empty($_POST['name']) ) {
					if ( $marksModels->addMark($_POST['name']) ) {
						$message['type'] = 'success';
						$message['msg'] = 'La marque a bien été ajoutée';
							
						$this->users->addTempVar('message', $message);
						$this->redirect( array('admin', 'truckmark') );
						return;
					}
					else {
						$message['type'] = 'danger';
						$message['msg'] = 'La marque n\'a pu être créer. Veuillez retenter.';
						$name = $_POST['name'];
					}
				}
				else {
					$message['type'] = 'danger';
					$message['msg'] = 'Vous indiquez un nom pour la marque';
				}
			}
				
			echo $this->twigRender('@Admin/marks_add.twig', array(
					'message' => $message,
					'name' => $name,
			));
		}
		else if ( substr($page, 0, 3) === 'del' ) {
			$id = substr($page, 4);
				
			if ( $marksModels->delMark($id) ) {
				$message['type'] = 'success';
				$message['msg'] = 'La marque a bien été supprimée';
					
				$this->users->addTempVar('message', $message);
				$this->redirect( array('admin', 'truckmark') );
			}
			else {
				$message['type'] = 'danger';
				$message['msg'] = 'Impossible de supprimer la marque';
				$this->redirect( array('admin', 'truckmark') );
			}
		}
		else {
			echo $this->twigRender('@Admin/marks_list.twig', array(
					'marks' => $marksModels->getMarks(),
					'message' => $message
			));
		}
	}
	
	private function trucks($page) {
		$trucksModels = new TrucksModels();
		$markModels = new TrucksMarkModels();
		$message = array();
		$name = '';
		$mark = 0;
		
		if ( $page === 'add' ) {
			$marks = $markModels->getMarks();
			
			if ( !empty($_POST) ) {
				if ( !empty($_POST['name']) && !empty($_POST['mark']) ) {
					if ( $trucksModels->addTruck( intval($_POST['mark']), $_POST['name']) ) {
						$message['type'] = 'success';
						$message['msg'] = 'Le camion a bien été ajouté';
							
						$this->users->addTempVar('message', $message);
						$this->redirect( array('admin', 'truck') );
						return;
					}
					else {
						$message['type'] = 'danger';
						$message['msg'] = 'Le camion n\'a pu être créer. Veuillez retenter.';
						$name = $_POST['name'];
						$i = 0;
						
						foreach ($marks as $key => $val) {
							if ( $_POST['mark'] === $marks[$i]->id_mark ) {
								$marks[$i]->select = true;
								break;
							}
							
							$i++;
						}
					}
				}
				else {
					$message['type'] = 'danger';
					$message['msg'] = 'Vous devez indiquer un nom pour le camion et une marque';
				}
			}
			
			echo $this->twigRender('@Admin/trucks_add.twig', array(
					'message' => $message,
					'name' => $name,
					'marks' => $marks
			));
		}
		else if ( substr($page, 0, 3) === 'del' ) {
				$id = substr($page, 4);
					
				if ( $trucksModels->delTruck($id) ) {
					$message['type'] = 'success';
					$message['msg'] = 'Le camion a bien été supprimée';
						
					$this->users->addTempVar('message', $message);
					$this->redirect( array('admin', 'truck') );
				}
				else {
					$message['type'] = 'danger';
					$message['msg'] = 'Impossible de supprimer le camion';
					$this->redirect( array('admin', 'truck') );
				}
			}
		else {
			$truckList = $trucksModels->getTrucks();
			$markList = $markModels->getMarks();
			$trucks = array();
			$i = 0;
			
			foreach ($truckList as $key => $val) {
				$trucks[$i]['id_truck'] = $val->id_truck;
				$trucks[$i]['id_mark'] = $val->id_mark;
				$trucks[$i]['name'] = $val->name;
				
				for ($j = 0; $j < count($markList); $j++) {
					if ( $markList[$j]->id_mark === $val->id_mark ) {
						$trucks[$i]['mark'] = $markList[$j]->name;
					}
				}
				
				$i++;
			}
			
			echo $this->twigRender('@Admin/trucks_list.twig', array(
				'message' => $message,
				'trucks' => $trucks,
			));
		}
	}
	
	private function saveConfig($post) {
		if ( $post->database === 'mysql' && !empty($post->dbname) && !empty($post->host) &&
			 !empty($post->user) && !empty($post->password) ) {
			$database = array(
				'dsn' => '\''.$post->database.':dbname='.$post->dbname.'; host='.$post->host.'\'',
				'user' => '\''.$post->user.'\'',
				'password' => '\''.$post->password.'\''
			);
			
			$string = '<?'.PHP_EOL.PHP_EOL;
			
			foreach ($database as $key => $val) {
				$string .= '$database[\''.$key.'\'] = '.$val.';'.PHP_EOL;
			}
			
			if ( !file_put_contents(LOCAL_PATH.'/includes/database.php', $string) ) {
				return array('type' => 'danger', 'msg' => 'La configuration n\'a pu être sauvegardée');
			}
			else {
				return array('type' => 'success', 'msg' => 'La configuration a bien été sauvegardée');
			}
		}
		else {
			return array('type' => 'danger', 'msg' => 'Vous devez compléter tout les champs du formulaire');
		}
	}
	
	private function loadConfig() {
		$string = file_get_contents(LOCAL_PATH.'/includes/database.php');
		$string = str_replace('<?php'.PHP_EOL.PHP_EOL, '', $string);
		eval($string);
		
		return $database;
	}
	
	/**
	 * List all users
	 */
	private function userList() {
		$users = new UsersModels();
		$usersList = $users->getAllUsers();
		$session = $this->users->getInstance();
		
		foreach ( $usersList  as $i => $user) {
			foreach ($user as $key => $val) {
				if ( $key === 'access' ) {
					if ( intval($val) === $session::ADMINISTRATOR ) {
						$usersList[$i]['access'] = 'Administrateur';
					}
					else if ( intval($val) === $session::BANNED ) {
						$usersList[$i]['access'] = 'Bannis';
					}
					else if ( intval($val) === $session::MEMBER ) {
						$usersList[$i]['access'] = 'Utilisateur';
					}
					else if ( intval($val) === $session::MODERATOR ) {
						$usersList[$i]['access'] = 'Modérateur';
					}
					else {
						$usersList[$i]['access'] = 'Inconnu';
					}
				}
			}
		}
		
		echo $this->twigRender('@Admin/users/list.twig', array(
			'users' => $usersList,
			'message' => $this->users->getTempVar('message')
		));
	}
	
	/**
	 * Change selected user
	 * @param integer $id
	 * <div>ID of user</div>
	 */
	private function editUser($id) {
		$message = array();
		$result = false;
		
		$usersModel = new UsersModels();
		
		if ( !empty($_POST) ) {
			if ( !empty($_POST['mail']) && !empty($_POST['login']) &&
				!empty($_POST['access']) ) {
				if ( $_POST['password'] !== $_POST['conf-password'] ) {
					$message['type'] = 'danger';
					$message['msg'] = 'Le mot de passe et sa confirmation ne concordent pas';
				}
				else {
					$_POST['id'] = $id;
					$result = $usersModel->changeUser($_POST);
					
					if ($result) {
						$message['type'] = 'success';
						$message['msg'] = 'L\'utilisateur <b>'.$id.'</b> a bien été modifié';
					}
					else {
						$message['type'] = 'danger';
						$message['msg'] = 'L\'utilisateur <b>'.$id.'</b> n\'a pas été modifié';
					}
				}
			}
		}
		
		$user = $usersModel->getUser($id);
		
		if ( $result ) {
			$this->redirect(array('admin', 'user', 'list'), 2);
		}
		
		echo $this->twigRender('@Admin/users/edit.twig', array(
			'message' => $message,
			'user' => $user,
		));
	}
}
