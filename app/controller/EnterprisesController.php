<?php

namespace Core;

use Core\AppController;

class EnterprisesController extends AppController {
	public function indexAction($args) {
		$data = array('gain_path' => 35, 'spend_impact' => 10, 'base_salary' => 1200);
		$message = array();
		
		$enterprisesModel = new EnterprisesModels();
		$entEmployeesModels = new EntEmployeesModels();
		$users = new UsersModels();
		
		if ( $args->type === 'add' ) {
			if ( !empty($_POST) ) {
				if ( !empty($_POST['tag']) && !empty($_POST['name']) && !empty($_POST['gain_path']) &&
					!empty($_POST['spend_impact']) && !empty($_POST['base_salary']) && strlen($_POST['tag']) === 3 ) {
					$result = $enterprisesModel->addEnterprise($_POST, $id);
					
					if ( $result ) {
						$message['type'] = 'success';
						$message['msg'] = 'L\'entreprise <b>'.$_POST['name'].'</b> a bien été créée';
						
						$entEmployeesModels->addEmployees( array(
							'id_ent' => $id,
							'id_users' => $this->users->getUser()->id,
							'access' => 0x02,
							'candidacy' => ''
						) );
						
						$this->users->addTempVar('message', $message);
						$this->redirect('enterprises');
					}
					else {
						$message['type'] = 'danger';
						$message['msg'] = 'L\'entreprise <b>'.$_POST['name'].'</b> n\'a pu être créée';
					}
				}
				else {
					$message['type'] = 'danger';
					$message['msg'] = 'Vous devez remplir tout les champs';
				}
			}
			
			echo $this->twigRender('@Core/ent_add.twig', array(
					'data' => (!empty($_POST)) ? $_POST : $data,
					'message' => $message
			));
		}
		else if ( $args->type === 'list' ) {
			
			$employee = $entEmployeesModels->getEmployee( $this->users->getUser()->id );
			$enterprise = $enterprisesModel->getEnterprise( $employee['id_ent'] );
			
			echo $this->twigRender('@Core/ent_list.twig', array(
				'enterprise' => $enterprise,
				'enterprises' => $enterprisesModel->getAllEnterprises(),
				'ent_access' => ($employee['access'] !== NULL) ? intval($employee['access']) : NULL,
				'id_ent' => intval($employee['id_ent'])
			));
		}
		else if ( $args->type === 'edit' ) {
			$enterprise = $enterprisesModel->getEnterprise( $args->id );
			$data = $enterprise;
			
			if ( !empty($_POST) ) {
				if ( !empty($_POST['gain_path']) && !empty($_POST['spend_impact']) &&
					!empty($_POST['base_salary']) ) {
					$data = $_POST;
					$data['id'] = $args->id;
					
					$result = $enterprisesModel->changeEnterprise($data);
					
					if ( $result ) {
						$message['type'] = 'success';
						$message['msg'] = 'L\'entreprise <b>'.$enterprise['name'].'</b> a bien été modifiée';
						
						$this->users->addTempVar('message', $message);
						$this->redirect( 'enterprises' );
						return;
					}
					else {
						$message['type'] = 'danger';
						$message['msg'] = 'L\'entreprise <b>'.$enterprise['name'].'</b> n\'a pu être modifiée';
						$data = $_POST;
						$data['id'] = $args->id;
					}
				}
				else {
					$message['type'] = 'danger';
					$message['msg'] = 'L\'entreprise <b>'.$enterprise['name'].'</b> n\'a pu être modifiée';
					$data = $_POST;
					$data['id'] = $args->id;
				}
			}
			
			echo $this->twigRender('@Core/ent_edit.twig', array(
				'data' => $data
			));
		}
		else if ( $args->type === 'del' ) {
			$result = $enterprisesModel->delEnterprise($args->id);
			
			if ( $result ) {
				$message['type'] = 'success';
				$message['msg'] = 'L\'entreprise a bien été supprimée';
				
				$result = $entEmployeesModels->delAllEmployee($args->id, $this->users->getUser()->id);
			}
			else {
				$message['type'] = 'danger';
				$message['msg'] = 'L\'entreprise n\'a pas pu être supprimée';
			}
			
			$this->users->addTempVar('message', $message);
			$this->redirect( 'enterprises' );
		}
		else if ( $args->type === 'join' ) {
			$data['id_enterprise'] = $args->id;
			
			if ( !empty($_POST) ) {
				if ( !empty($_POST['candidacy']) ) {
					$data['id_users'] = $this->users->getUser()->id;
					$data['access'] = 0x00;
					$data['candidacy'] = $_POST['candidacy'];
					$data['id_ent'] = $args->id;
					
					$result = $entEmployeesModels->addEmployees($data);
					
					if ( $result ) {
						$this->redirect('enterprises');
					}
				}
			}
			
			echo $this->twigRender('@Core/ent_join.twig', array(
				'data' => $data
			));
		}
		else if ( $args->type === 'candidacy' && $args->id !== NULL ) {
			$usersList = $users->getAllUsers();
			$candidacies = $entEmployeesModels->getAllEmployees();
			$array = array();
			$i = 0;
			
			foreach ($candidacies as $candidacyKey => $candidacyValue) {
				if ( $candidacies[$candidacyKey]['access'] === '0' &&
					$candidacies[$candidacyKey]['id_ent'] === $args->id ) {
					foreach ($usersList as $userKey => $userValue) {
						if ( $usersList[$userKey]['id_users'] === $candidacies[$candidacyKey]['id_users'] ) {
							$array[$i]['employee'] = $usersList[$userKey]['login'];
							$array[$i]['text'] = $candidacies[$candidacyKey]['candidacy'];
							$array[$i]['id_user'] = $usersList[$userKey]['id_users'];
							$array[$i]['id_ent'] = $args->id;
							$i++;
						}
					}
				}
			}
			
			echo $this->twigRender('@Core/ent_candidacy.twig', array(
				'candidacies' => $array,
					'message' => $message
			));
		}
		else if ( $args->type === 'candidacy-accept' && $args->id !== NULL && $args->ent !== NULL ) {
			$result = $entEmployeesModels->candidacyAccepted($args->id);
			if ( $result ) {
				$message['type'] = 'success';
				$message['msg'] = 'La candidature a bien été acceptée';
				
				$this->users->addTempVar('message', $message);
			}
			
			$this->redirect( array('enterprises', 'candidacy', $args->ent) );
		}
		else if ( $args->type === 'candidacy-cancel' && $args->id !== NULL && $args->ent !== NULL ) {
			$result = $entEmployeesModels->candidacyRejected($args->id);
			if ( $result ) {
				$message['type'] = 'success';
				$message['msg'] = 'La candidature a bien été supprimée';
		
				$this->users->addTempVar('message', $message);
			}
				
			$this->redirect( array('enterprises', 'candidacy', $args->ent) );
		}
		else if ( $args->type === 'quit' ) {
			$entEmployeesModels->delEmployee($args->id, $this->users->getUser()->id);
			$this->redirect('enterprises');
		}
		else if ( $args->type === 'employees' ) {
			$result = $entEmployeesModels->getAllEmployees();
			$employees = array();
			$i = 0;
			
			foreach ( $result  as $key => $val) {
				if ( $val['id_ent'] === $args->id && intval($val['access']) > 0 ) {
					$employees[$i]['id_users'] = intval($val['id_users']);
					$i++;
				}
			}
			
			for ($i = 0; $i < count($employees); $i++) {
				$employees[$i]['login'] = $users->getUser($employees[$i]['id_users'])->login;
			}
			
			echo $this->twigRender('@Core/employees_list.twig', array(
				'employees' => $employees,
				'id_ent' => $args->id,
				'ent_access' => $this->users->getUser()->access_ent,
				'id_user' => $this->users->getUser()->id
			));
		}
		else if ( $args->type === 'disposal' ) {
			$result = $entEmployeesModels->delEmployee($args->ent,  $this->users->getUser()->id );
			$entEmployeesModels->changeEmployee($args->id, 2);
			
			$this->redirect( 'enterprises' );
		}
		else if ( $args->type === 'addpath' ) {
			$reportPathsModels = new ReportPathsModels();
			$trucksModels = new TrucksModels();
			$trucksMarkModels = new TrucksMarkModels();
			$trailersModels = new TrailersModels();
			$citiesModels = new CitiesModels();
			
			if ( !empty($_POST) ) {
				$_POST['id_user'] = $this->users->getUser()->id;
				$_POST['id_ent'] = $args->id;
				
				$result = $reportPathsModels->addPath($_POST);
				
				if ( $result ) {
					$message['type'] = 'success';
					$message['msg'] = 'Votre trajet a bien été enregistré';
					$this->users->addTempVar('message', $message);
					$this->redirect( array('enterprises', 'listpath') );
					return;
				}
				else {
					$message['type'] = 'danger';
					$message['msg'] = 'Impossible d\'enregistrer le trajet';
					$this->users->addTempVar('message', $message);
				}
			}
			
			echo $this->twigRender('@Core/path_report.twig', array(
				'message' => $message,
				'id' => $args->id,
				'cities' => $citiesModels->getCities(),
				'trucks' => $trucksModels->getTrucks(),
				'marks' => $trucksMarkModels->getMarks(),
				'trailers' => $trailersModels->getTrailers()
			));
		}
		else if ( $args->type === 'listpath' ) {
			$paths = array();
			$i = 0;
			
			$reportPathsModels = new ReportPathsModels();
			$trucksModels = new TrucksModels();
			$trucksMarkModels = new TrucksMarkModels();
			$trailersModels = new TrailersModels();
			$citiesModels = new CitiesModels();
			$usersModels = new UsersModels();
			$entEmployeesModels = new EntEmployeesModels();
			$enterpriseModel = new EnterprisesModels();
			
			$cities = $citiesModels->getCities();
			$trucks = $trucksModels->getTrucks();
			$marks = $trucksMarkModels->getMarks();
			$trailers = $trailersModels->getTrailers();
			$employee = $entEmployeesModels->getEmployee( $this->users->getUser()->id );
			$reports = $reportPathsModels->getPaths($employee['id_ent']);
			$enterprise = $enterprisesModel->getEnterprise( $employee['id_ent'] );
			
			if ( !empty($args->id) ) {
				$id = intval($args->id);
			}
			else {
				$id = $this->users->getUser()->id;
			}
			
			$username = $usersModels->getUser($id)->login;
			
			foreach ($reports as $key => $val) {
				if ( intval($val->id_user) === $id ) {
					$paths[$i]['date'] = $val->date;
					
					foreach ($cities as $value) {
						if ( intval($val->city_start) === intval($value->id_city) ) {
							$paths[$i]['city_start'] = $value->name;
						}
						
						if ( intval($val->city_end) === intval($value->id_city) ) {
							$paths[$i]['city_end'] = $value->name;
						}
					}
					
					foreach ($marks as $value) {
						if ( intval($val->truck_mark) === intval($value->id_mark) ) {
							$paths[$i]['mark'] = $value->name;
						}
					}
					
					foreach ($trucks as $value) {
						if ( intval($val->truck_model) === intval($value->id_truck) ) {
							$paths[$i]['model'] = $value->name;
						}
					}
					
					foreach ($trailers as $value) {
						if ( intval($val->cargo) === intval($value->id) ) {
							$paths[$i]['cargo'] = $value->name; //.' ('.$value->weight.')';
						}
					}
					
					$paths[$i]['dist'] = $val->traveled_dist;
					$paths[$i]['profit'] = $val->profit;
					$paths[$i]['xp'] = $val->xp;
					$paths[$i]['fuel'] = $val->fuel;
					$paths[$i]['comments'] = $val->comments;
					
					$i++;
				}
			}
			
			echo $this->twigRender('@Core/path_report_list.twig', array(
				'paths' => $paths,
				'username' => $username,
				'yourPath' => (is_null($args->id)) ? true : false,
				'enterprise' => $enterprise
			));
		}
	}
}
