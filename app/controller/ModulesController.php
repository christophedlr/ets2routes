<?php

namespace Core;

/**
 * Modules controller
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @author Christophe Daloz - De Los Rios <christophedlr@gmail.com>
 * @copyright ETS2Routes Dev Team 2016
 */
class ModulesController {
	private static $instance;
	private $dom;
	private $db;
	private static $init = false;
	
	private function __construct() {
		$this->dom = new \DOMDocument();
		$this->db = new ModulesModels();
		$this->initModules();
	}
	
	public static function getInstance() {
		if ( is_null(self::$instance) ) {
			self::$instance = new ModulesController();
		}
	
		return self::$instance;
	}
	
	/**
	 * Call module with controller information
	 * 
	 * @param string $controller
	 * <div>Controller information with syntaxe: namespace:controller:MethodAction</div>
	 * 
	 * @return boolean
	 * <div>True if module has been call or false if not</div>
	 */
	public function callModule($controller) {
		foreach ($this->db->getActivatesModules() as $value) {
			if ( $value['principal'] === $controller ) {
				$explode = explode(':', $controller);
				
				$class = $explode[0].'\\'.$explode[1];
				$ctr = new $class();
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Initialization of modules
	 */
	public function initModules() {
		foreach ($this->db->getActivatesModules() as $value) {
			$explode = explode(':', $value['init']);
			$class = $explode[0].'\\'.$explode[1];
			$ctr = new $class();
			
			$action = $explode[2];
			if ( method_exists($ctr, $action) ) {
				var_dump('test');
				$ctr->$action();
			}
		}
	}
	
	/**
	 * Get module name with directory database information
	 * 
	 * @param string $dir
	 * <div>Directory name of module</div>
	 * 
	 * @return string
	 * <div>Name of module or empty string</div>
	 */
	public function getName($dir) {
		foreach ($this->db->getActivatesModules() as $value) {
			if ( $value['dir'] === $dir ) {
				return $value['name'];
			}
		}
		return '';
	}
}
