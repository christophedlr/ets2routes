<?php

namespace Core;

/**
 * Hook controller for add fonctionalities from modules
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @author Christophe Daloz - De Los Rios <christophedlr@gmail.com>
 * @copyright ETS2Routes Dev Team 2016
 */
class HookController {
	private static $instance = null;
	private $list;
	
	private function __construct() {
		$this->list['css'] = array();
		$this->list['after_body'] = array();
		$this->list['before_body'] = array();
	}
	
	/**
	 * Get instance of HookController
	 * @return \Core\HookController
	 */
	public static function getInstance() {
		if ( is_null(self::$instance) ) {
			self::$instance = new HookController();
		}
		
		return self::$instance;
	}
	
	/**
	 * Add fonctionalities from modules
	 * @param string $actionName
	 * <div>Name of action, composed by controllername_action</div>
	 * 
	 * @param string|array $action
	 * <div>Function action or string action (URL for CSS by example)</div>
	 * 
	 * @param string|null $area
	 * <div>Area of insert action (add template). By default, area is null</div>
	 */
	public function addAction($actionName, $action, $area = null) {
		$explode = explode('_', $actionName);
		$ctr_file = ucfirst($explode[0]).'Controller';
		
		switch ( $explode[1] ) {
			case 'tpl':
				$result = call_user_func( $action );
				$this->addTemplate($ctr_file, $result, $area);
			break;
			
			case 'css':
				$this->addCSS($ctr_file, $action);
			break;
		}
	}
	
	/**
	 * Add new CSS in selected controller
	 * @param string $ctr
	 * <div>Controller to add CSS</div>
	 * 
	 * @param string $url
	 * <div>URL of CSS</div>
	 */
	private function addCSS($ctr, $url) {
		$this->list['css'][$ctr][] = $url;
	}
	/**
	 * Add template in selected controller
	 * @param string $ctr
	 * <div>Controller to add template</div>
	 * 
	 * @param string $data
	 * <div>Template at add in controller</div>
	 * 
	 * @param string $area
	 * <div>after or before controller add</div>
	 */
	private function addTemplate($ctr, $data, $area) {
		if ( $area === 'before' ) {
			$this->list['before_body'][$ctr][] = $data;
		}
		else if ( $area === 'after' ) {
			$this->list['after_body'][$ctr][] = $data;
		}
	}
	
	/**
	 * Get list of hook
	 * @param string $element
	 * <div>Name of hook element</div>
	 */
	public function getList($element) {
		if ( isset($this->list[$element]) ) {
			return $this->list[$element];
		}
	}
}
