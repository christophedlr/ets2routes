<?php

namespace Core;

use Core\AppController;

class ErrorController extends AppController {
	public function mDisabledAction($controller) {
		$explode = explode(':', $controller);
		
		echo $this->twigRender('@Core/error.twig', array(
				'error_code' => 'module',
				'text' => $explode[1]
		));
	}
}
