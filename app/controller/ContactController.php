<?php

namespace Core;

use Core\AppController;

/**
 * Contact controller
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @author Christophe Daloz - De Los Rios <christophedlr@gmail.com>
 * @copyright ETS2Routes Dev Team 2016
 */
class ContactController extends AppController {
	/**
	 * Index contact action. Display contact form
	 */
	public function indexAction() {
		$message = array();
		
		if ( !empty($_POST) ) {
			$message = $this->sendMailAction();
		}
		
		$form = new \Form();
		
		$form->open( APP_VIEWS_PATH.'/forms/contact.html' );
		$form->setAttribute($form->getElementsByTagName('form')->item(0),
				'action', ABSOLUTE_PATH.'/contact');
		
		$span = $form->createNode('span');
		$span->setAttribute('class', 'required');
		$span->nodeValue = '*';
		
		$form->getElementsByTagName('label')->item(0)->nodeValue = 'Votre e-mail :&nbsp;';
		$form->getElementsByTagName('label')->item(0)->appendChild($span);
		
		$span = $form->createNode('span');
		$span->setAttribute('class', 'required');
		$span->nodeValue = '*';
		
		$form->getElementsByTagName('label')->item(1)->nodeValue = 'Objet :&nbsp;';
		$form->getElementsByTagName('label')->item(1)->appendChild($span);
		
		$span = $form->createNode('span');
		$span->setAttribute('class', 'required');
		$span->nodeValue = '*';
		
		$form->getElementsByTagName('label')->item(2)->nodeValue = 'Message :&nbsp;';
		$form->getElementsByTagName('label')->item(2)->appendChild($span);
		$form->getElementById('submit')->nodeValue = 'Envoyer';
		
		echo $this->twigRender('@Core/contact.twig', array(
			'form' => $form->render(),
			'message' => $message
		));
	}
	
	/**
	 * Sending mail
	 * @return message
	 */
	private function sendMailAction() {
		if ( !empty($_POST['mail']) && !empty($_POST['object']) && !empty($_POST['message']) ) {
			$from = $_POST['mail'];
			$to = 'root@chrislocal.fr.nf';
			$subject = $_POST['object'];
			$message = $_POST['message'];
			
			$mail = new \PHPMailer();
			
			$mail->addCustomHeader('X-Priority: 3');
			$mail->addCustomHeader('Reply-To', $from);
			$mail->addCustomHeader('Priority', 'normal');
			$mail->From = $from;
			$mail->Subject = $subject;
			$mail->addAddress($to);
			$mail->Body = $message;
			$mail->ContentType = 'text/plain';
			$mail->CharSet = 'UTF-8';
			$mail->encodeString('8bit');
			
			if ( $mail->send() ) {
				$msg['type'] = 'success';
				$msg['msg'] = 'Votre e-mail a bien été envoyée.';
			}
			else {
				$msg['type'] = 'danger';
				$msg['msg'] = 'Votre e-amail n\'a pu être transmis. Veuillez retenter plus tard.';
			}
		}
		else {
			$msg['type'] = 'danger';
			$msg['msg'] = 'vous n\'avez pas remplis le formulaire.';
		}
		
		return $msg;
	}
}
