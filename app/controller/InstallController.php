<?php

namespace Core;

use Core\AppController;

class InstallController extends AppController {
	public function indexAction() {
		$message = array();
		
		if ( !empty($_POST) ) {
			$array = $this->checkForm($_POST);
			$database = array(
				'dbname' => $array[0]['dbname'],
				'host' => $array[0]['host'],
				'user' => $array[0]['user-sql'],
				'password' => $array[0]['password-sql']
			);
			$user = array(
				'name' => $array[0]['username'],
				'password' => $array[0]['password'],
				'mail' => $array[0]['mail']
			);
		}
		
		echo $this->twigRender('@Core/install.twig', array(
			'message' => (!empty($array)) ? $array[1] : $message,
			'database' => (!empty($database)) ? $database : array(),
			'user' => (!empty($user)) ? $user : array(),
			
		));
	}
	
	private function checkForm($data) {
		global $database;
		
		if ( !empty($data['database']) && !empty($data['dbname']) && !empty($data['host'] &&
			 !empty($data['user-sql']) && !empty($data['password-sql']) && !empty($data['username']) &&
			 !empty($data['password']) && !empty($data['mail'])) ) {
			try {
				$database['dsn'] = 'mysql:dbname='.$data['dbname'].'; host='.$data['host'];
				$database['user'] = $data['user-sql'];
				$database['password'] = $data['password-sql'];
				
				$post['database'] = $data['database'];
				$post['host'] = $data['host'];
				$post['dbname'] = $data['dbname'];
				$post['user'] = $data['user-sql'];
				$post['password'] = $data['password-sql'];
				
				$config = new ConfigModels();
				$message = $config->save( (object)$post );
				
				if ( $message['type'] === 'success' ) {
					$install = new InstallModels();
					if ( $install->dropTables() ) {
						if ( $install->createTables() ) {
							if ( $install->insertDatas() ) {
								$user = array(
									'login' => $data['username'],
									'password' => $data['password'],
									'mail' => $data['mail'],
									'access' => 16
								);
								
								$users = new UsersModels();
								if ( $users->setUser( $user ) ) {
									$message['type'] = 'success';
									$message['msg'] = 'L\'installation s\'est bien passée,
										vous pouvez maintenant vous connecter avec vos identifiants';
									$this->users->addTempVar('message', $message);
									$this->redirect('index');
									
									return;
								}
								else {
									$message['type'] = 'danger';
									$message['msg'] = 'Impossible de créer l\'utilisateur, merci de refaire l\'installation';
									return array($data, $message);
								}
							}
							else {
								$message['type'] = 'danger';
								$message['msg'] = 'Impossible d\'enregistrer les
								données de base, merci de refaire l\'installation';
								return array($data, $message);
							}
						}
						else {
							$message['type'] = 'danger';
							$message['msg'] = 'Impossible de crééer les tables, merci de refaire l\'installation';
							return array($data, $message);
						}
					}
					else {
						$message['type'] = 'danger';
						$message['msg'] = 'Impossible de supprimer les tables existantes, merci de refaire l\'installation';
						return array($data, $message);
					}
				}
				else {
					return array($data, $message);
				}
			} catch (\PDOException $e) {
				$message['type'] = 'danger';
				$message['msg'] = 'Les informations de connexion à la base ne sont pas bons';
				
				return array($data, $message);
			}
		}
		else {
			$message['type'] = 'danger';
			$message['msg'] = 'Vous devez remplir tout le formulaire';
			
			return array($data, $message);
		}
	}
}
