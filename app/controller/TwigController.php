<?php

namespace Core;

/**
 * Twig controller
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @author Christophe Daloz - De Los Rios <christophedlr@gmail.com>
 * @copyright ETS2Routes Dev Team 2016
 */
class TwigController {
	private static $instance = null;
	protected $template;
	protected $loader;
	
	private function __construct() {
		$this->loader = new \Twig_Loader_Filesystem();
		$this->template = new \Twig_Environment($this->loader);
		
		$this->template->addExtension( new \Twig_Extension_StringLoader() );
	}
	
	/**
	 * Get instance of HookController
	 * @return \Core\TwigController
	 */
	public static function getInstance() {
		if ( is_null(self::$instance) ) {
			self::$instance = new TwigController();
		}
	
		return self::$instance;
	}
	
	/**
	 * Get Twig template engine
	 * 
	 * @return Twig_Environment Twig engine
	 */
	public function getTemplate() {
		return $this->template;
	}
	
	/**
	 * Register new base directory for search template for Twig engine
	 *
	 * @param string $directory
	 * <div>Name of directory</div>
	 *
	 * @param string|null $name
	 * <div>Name of desired namespace for access to template</div>
	 */
	public function addTwigBaseDir($directory, $name = NULL) {
		if ( !is_null($name) ) {
			$this->loader->addPath($directory.'/views', $name);
		}
		else {
			$this->loader->addPath($directory.'/views');
		}
	}
	
	/**
	 * Add new twig var
	 * 
	 * @param string $var
	 * <div>Name of variable in Twig templates</div>
	 * 
	 * @param mixed $action
	 * <div>Action for used new variable</div>
	 */
	public function addTwigVar($var, $action) {
		$this->getTemplate()->addGlobal($var, $action);
	}
	
	/**
	 * Add new Twig function
	 * 
	 * @param string $name
	 * <div>Name of function in Twig templates</div>
	 * 
	 * @param function $function
	 * <div>Function call</div>
	 */
	public function addTwigFunction($name, $function) {
		$this->getTemplate()->addFunction( new \Twig_SimpleFunction($name, $function) );
	}
	
	/**
	 * Add new Twig filter
	 *
	 * @param string $name
	 * <div>Name of filter in Twig templates</div>
	 *
	 * @param function $function
	 * <div>Function call</div>
	 */
	public function addTwigFilter($name, $function) {
		$this->getTemplate()->addFilter( new \Twig_SimpleFilter($name, $function) );
	}
}
