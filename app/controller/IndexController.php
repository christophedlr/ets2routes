<?php

namespace Core;

use Modules\News\NewsController;

/**
 * Index controller
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @author Christophe Daloz - De Los Rios <christophedlr@gmail.com>
 * @copyright ETS2Routes Dev Team 2016
 */
class IndexController extends AppController {
	/**
	 * Index Action
	 */
	public function indexAction() {
		if ( !file_exists('includes/database.php') ) {
			$this->redirect('install');
		}
		else {
			new NewsController();
			echo $this->twigRender( '@Core/index.twig', array('message' => $this->users->getTempVar('message')) );
		}
	}
}
