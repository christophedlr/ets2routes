<?php

namespace Core;

use Core\Users\SessionController;

/**
 * Application controller
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @author Christophe Daloz - De Los Rios <christophedlr@gmail.com>
 * @copyright ETS2Routes Dev Team 2016
 */
class AppController {
	protected $twig;
	protected $modules;
	protected $hook;
	protected $users;
	
	public function __construct() {
		global $modules;
		
		$this->twig = TwigController::getInstance();
		$this->users = SessionController::getInstance();
		
		$this->modules = $modules;
		$this->hook = HookController::getInstance();
		
		$this->list['css'] = array();
		
		$this->twig->addTwigBaseDir('app', 'Core');
		
		$this->twig->addTwigVar('css_path', CSS_PATH);
		$this->twig->addTwigVar('img_path', IMG_PATH);
		$this->twig->addTwigVar('js_path', JS_PATH);
		$this->twig->addTwigVar('path', ABSOLUTE_PATH);
		$this->twig->addTwigVar('session', $this->users->getUser());
		$this->twig->addTwigVar('access', $this->users->getConstants());
		
		$this->twig->addTwigFunction('path', function() {
			$arrays = func_get_args();
			$args = '';
			
			foreach ($arrays as $key => $val) {
				$args .= '/'.$val;
			}
			
			return ABSOLUTE_PATH.$args;
		});
		
		$this->twig->addTwigFilter('entities', function($string) {
			return htmlentities($string, ENT_COMPAT, 'UTF-8');
		});
	}
	
	/**
	 * Get Twig template engine
	 * 
	 * @return Twig_Environment Twig engine
	 */
	public function getTemplate() {
		return $this->twig->getTemplate();
	}
	
	/**
	 *  Twig template render
	 * @param string $anme
	 * <div>Name of template</div>
	 * 
	 * @param array $context
	 * <div>Array containing var => value</div>
	 */
	public function twigRender($name, $context) {
		$explode = explode('\\', get_called_class() );
		
		foreach ($this->hook->getList('after_body') as $key => $val) {
			if ( $key === $explode[ count($explode)-1 ] ) {
				$context['after_body'][] = $val[0];
			}
		}
		
		foreach ($this->hook->getList('before_body') as $key => $val) {
			if ( $key === $explode[ count($explode)-1 ] ) {
				$context['before_body'][] = $val[0];
			}
		}
		
		foreach ($this->hook->getList('css') as $key => $val) {
			if ( $key === $explode[ count($explode)-1 ] ) {
				$context['csslist'][] = $val[0];
			}
		}
		
		return $this->getTemplate()->render($name, $context);
	}
	
	/**
	 * Redirection to selected route
	 * @param string|array $name
	 * <div>Name of route or name of route and list of parameters</div>
	 * 
	 * @param integer $time
	 * <div>Number of waiting seconds</div>
	 */
	public function redirect($name, $time = 0) {
		global $urlGenerator;
		
		$url = '';
		$route = $name;
		
		if ( is_array($name) ) {
			$route = $name[0];
			array_shift($name);
			$url = $urlGenerator->generate($route).'/'.implode('/', $name);
		}
		else {
			$url = $urlGenerator->generate($route);
		}
		
		header('Refresh: '.$time.'; url='.ABSOLUTE_PATH.$url);
	}
}
