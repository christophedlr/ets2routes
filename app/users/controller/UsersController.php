<?php

namespace Core\Users;

use Core\AppController;
use Core\UsersModels;
use Core\EntEmployeesModels;
use Core\EnterprisesModels;

/**
 * Users Controller
 * @license http://cecill.info/licences/Licence_CeCILL_V2-en.html CeCILL V2
 * @author Christophe Daloz - De Los Rios <christophedlr@gmail.com>
 * @copyright ETS2Routes Dev Team 2016
 */
class UsersController extends AppController {
	public function __construct() {
		parent::__construct();
		
		$this->twig->addTwigBaseDir('app/users', 'Users');
	}
	
	/**
	 * Index action of controller
	 * @param array $args
	 * <div>List of arguments for method</div>
	 */
	public function indexAction($args) {
		$message = array();
		
		switch ($args->type) {
			case 'register':
				if ( !empty($_POST) ) {
					if ( !empty($_POST['login']) && !empty($_POST['password']) && !empty($_POST['mail']) ) {
						$usersModels = new UsersModels();
						if ( $usersModels->setUser($_POST) ) {
							$message['type'] = 'success';
							$message['msg'] = 'Vous vous êtes bien inscrit. Bienvenue sur ETS2Routes.';
						}
						else {
							$message['type'] = 'danger';
							$message['msg'] = 'Une erreur est survenue lors de votre inscription.
								Si le problème persiste, merci de contacter l\'administrateur du site.';
						}
					}
					else {
						$message['type'] = 'danger';
						$message['msg'] = 'Vous devez remplir les champs avec une astérisque rouge';
					}
				}
				
				$form = new \Form();
				$form->open( 'app/users/views/forms/register.html' );
				$form->setAttribute($form->getElementsByTagName('form')->item(0),
					'action', ABSOLUTE_PATH.'/user/register');
				
				echo $this->twigRender('@Users/register.twig', array(
					'form' => $form->render(),
					'message' => $message
				));
				
				if ( !empty($message) && $message['type'] === 'success') {
					$this->redirect('index', 3);
				}
			break;
			
			case 'connection':
				if ( !empty($_POST) ) {
					if ( !empty($_POST['login']) && !empty($_POST['password'])) {
						$usersModels = new UsersModels();
						$object = $usersModels->getUserByLogin($_POST['login']);
						
						if ( $object !== false ) {
							if ( hash('sha256', $_POST['password']) === $object->password ) {
								$employee = new EntEmployeesModels();
								$ent = $employee->getEmployee($object->id_users);
								
								$this->users->setUser($object->id_users, $object->access,
									$object->login, $ent['id_ent'], $ent['access']);
								
								$message['type'] = 'success';
								$message['msg'] = 'Bienvenue <b>'.$object->login.'</b>';
							}
							else {
								$message['type'] = 'danger';
								$message['msg'] = 'Mauvais nom d\'utilisateur/mot de passe.';
							}
						}
						else {
							$message['type'] = 'danger';
							$message['msg'] = 'Vous devez remplir les champs avec une astérisque rouge.';
						}
					}
					else {
						$message['type'] = 'danger';
						$message['msg'] = 'Vous devez remplir les champs avec une astérisque rouge.';
					}
				}
				
				$form = new \Form();
				$form->open('app/users/views/forms/register.html');
				
				$form->setAttribute($form->getElementsByTagName('form')->item(0),
						'action', ABSOLUTE_PATH.'/user/connection');
				
				$mail = $form->getElementById('form_mail');
				$form->removeChildNode($form->getElementsByTagName('form')->item(0), $mail);
				
				$button = $form->getElementsByTagName('button')->item(0);
				$button->nodeValue = 'Se connecter';
				
				echo $this->twigRender('@Users/connection.twig', array(
					'form' => $form->render(),
					'message' => $message
				));
				

				if ( !empty($message) && $message['type'] === 'success') {
					$this->redirect('index', 3);
				}
			break;
			
			case 'disconnect' :
				unset($_SESSION['ets2routes']);
				session_destroy();
				$this->redirect('index', 0);
			break;
			
			case 'profile':
				$usersModels = new UsersModels();
				$result = $usersModels->getUser( $this->users->getInstance()->getUser()->id );
				
				if ( !empty($_POST) ) {
					$_POST['id'] = $this->users->getInstance()->getUser()->id;
					$_POST['access'] = $this->users->getInstance()->getUser()->access;
					
					$result = $usersModels->changeUser($_POST);
					
					if ( $result ) {
						$message['type'] = 'success';
						$message['msg'] = 'Votre profil a bien été mis à jour';
						
						$this->users->addTempVar('message', $message);
						$this->redirect('index', 2);
					}
					else {
						$message['type'] = 'danger';
						$message['msg'] = 'Vous n\'avez pas remplis le formulaire';
					}
				}
				
				echo $this->twigRender('@Users/profile.twig', array(
					'message' => $message,
					'user' => $result
				));
			break;
		}
	}
}
