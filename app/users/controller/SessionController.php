<?php

namespace Core\Users;

use Core\UsersModels;

class SessionController {
	private static $instance;
	const GUEST = 0x01;
	const MEMBER = 0x02;
	const BANNED = 0x04;
	const MODERATOR = 0x08;
	const ADMINISTRATOR = 0x10;
	
	private function __construct() {
		session_start();
		
		if ( !isset($_SESSION['ets2routes']['user']) || is_null($_SESSION['ets2routes']['user']) ) {
			$_SESSION['ets2routes']['user'] = (object)array(
				'id' => 0,
				'login' => 'anonymous',
				'access' => 0x01
			);
		}
	}
	
	public function getUser() {
		return $_SESSION['ets2routes']['user'];
	}
	
	public function setUser($id, $access, $login, $id_enterprise = NULL, $access_ent = NULL) {
		$_SESSION['ets2routes']['user'] = (object)array(
			'id' => intval($id),
			'access' => intval($access),
			'login' => $login,
			'id_enterprise' => intval($id_enterprise),
			'access_ent' => intval($access_ent)
		);
	}
	
	public static function getInstance() {
		if ( is_null(self::$instance) ) {
			self::$instance = new SessionController();
		}
	
		return self::$instance;
	}
	
	public static function getConstants() {
		$class = new \ReflectionClass(__CLASS__);
		return $class->getConstants();
	}
	
	/**
	 * Add a temporary variable in session
	 * For exemple: store message in session for redirection and display message
	 * 
	 * @param string $name
	 * <div>Name of variable</name>
	 * 
	 * @param mixed $var
	 * <div>The variable at store in session</div>
	 */
	public function addTempVar($name, $var) {
		$_SESSION['ets2routes']['temp'][$name] = $var;
	}
	
	/**
	 * Get a temporary variable and destroy session variable
	 * @param string $name
	 * <div>Name of variable</div>
	 * 
	 * @return mixed
	 * <div>Variable store in session or NULL if not exists</div>
	 */
	public function getTempVar($name) {
		if ( isset($_SESSION['ets2routes']['temp'][$name]) ) {
			$temp = $_SESSION['ets2routes']['temp'][$name];
			unset($_SESSION['ets2routes']['temp'][$name]);
			return $temp;
		}
		
		return NULL;
	}
	
	public function addVar($name, $var) {
		$_SESSION['ets2routes']['vars'][$name] = $var;
	}
	
	public function getVar($name) {
		if ( $_SESSION['ets2routes']['vars'][$name] ) {
			return $_SESSION['ets2routes']['vars'][$name];
		}
		
		return NULL;
	}
}
